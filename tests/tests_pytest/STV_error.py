#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# use bin() in python to convert to binary, or % 2 to determine if the last bit of the State Vector (h(t) ok) is on, 54759354 & 35797435 gives binary (1 only of both are 1s in that place)

import numpy as np

def bit_mask():
        fail_times = []
        standard = np.loadtxt('tests/tests_pytest/H1_Standard_State_Vector.txt')
        data = np.loadtxt('tests/tests_pytest/H1_State_Vector.txt')
        standard = np.transpose(standard)
        data = np.transpose(data)
        time = data[0]
        standard = standard[1]
        data = data[1]
        e_file = open('tests/tests_pytest/error_results.txt', 'a')
        for i in range(len(standard)):
                int_standard = int(standard[i])
                int_data = int(data[i])
                bin_standard = bin(int(int_standard))
                bin_data = bin(int(int_data))
                str_standard = str(bin_standard)[-1]
                str_data = str(bin_data)[-1]
                bits = int(str_standard) & int(str_data)
                if int_data % 2 != int_standard % 2:
                #if bits % 2 == 0:
                        fail_times.append(int(time[i]))
        e_file.write('h(t) not on for both: ' + str(fail_times) + '\n')
        assert len(fail_times) == 0
