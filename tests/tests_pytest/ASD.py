#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import numpy as np
import unittest
from gstlalcalibration import calibration_parts
from gstlal import pipeparts
from gstlalcalibration import test_common

from tests.tests_pytest.error import rms
from tests.tests_pytest.plot import plot_ASD

from lal import LIGOTimeGPS
from ligo import segments

from optparse import OptionParser, Option
import configparser

#from utils import common

parser = OptionParser()

parser.add_option("--gps-start-time", metavar = "seconds", type = int, default = 1370674240, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, default = 1370678400, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", default = "H1", help = "Name of the interferometer (IFO), e.g., H1, L1")

options, filenames = parser.parse_args()

ifo = options.ifo
#ifo = 'H1'

def compute_ASD_from_cache(pipeline, name):

        data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/GDS_frames.cache", cache_dsc_regex = ifo)
        data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, [(ifo, "GDS-CALIB_STRAIN"), (ifo, "GDS-CALIB_STRAIN_CLEAN")])))
        hoft = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STRAIN", ifo, 1.0, element_name_suffix = "0")
        #hoft = pipeparts.mktee(pipeline, hoft)
        #pipeparts.mknxydumpsink(pipeline, hoft, "tests/tests_pytest/ASD_data/hoft.txt")
        clean = calibration_parts.hook_up(pipeline, data, "GDS-CALIB_STRAIN_CLEAN", ifo, 1.0, element_name_suffix = "1")
        hoft = pipeparts.mkprogressreport(pipeline, hoft, "hoft")
        clean = pipeparts.mkprogressreport(pipeline, clean, "clean")
        pipeparts.mkgeneric(pipeline, hoft, "lal_asd", filename = "tests/tests_pytest/ASD_data/hoft_asd.txt", fft_samples = 16 * 16384, overlap_samples = int(2 * 16384))
        pipeparts.mkgeneric(pipeline, clean, "lal_asd", filename = "tests/tests_pytest/ASD_data/clean_asd.txt", fft_samples = 16 * 16384, overlap_samples = int(2 * 16384))

	#
	# done
	#

        return pipeline


#class TestASD:
#        """ASD test class"""

#def test_ASD():
#        """Test ASD"""

def ASD():
    test_common.build_and_run(compute_ASD_from_cache, "compute_ASD_from_cache", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))
    rms('H')
    rms('C')
    plot_ASD()

