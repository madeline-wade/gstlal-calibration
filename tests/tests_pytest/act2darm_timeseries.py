#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import numpy
from math import pi
from math import erf
import resource
import datetime
import glob

import configparser

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlalcalibration import test_common

#from utils import common

from tests.tests_pytest.error import rms
from tests.tests_pytest.plot import plot_act

from optparse import OptionParser, Option
import configparser

parser = OptionParser()

parser.add_option("--gps-start-time", metavar = "seconds", type = int, default = 1370674240, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, default = 1370678400, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", default = "H1", help = "Name of the interferometer (IFO), e.g., H1, L1")

options, filenames = parser.parse_args()

ifo = options.ifo


#ifo = 'H1'

#
# Load in the filters file that contains filter coefficients, etc.
#

#filters = numpy.load("tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1.npz")
filters = numpy.load("tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz")

# Set up gstlal frame cache list
#gstlal_frame_cache_list = ['tests/tests_pytest/filters/GDS_Approx_frames.cache', 'tests/tests_pytest/filters/GDS_Exact_frames.cache']
gstlal_frame_cache_list = ['tests/tests_pytest/GDS_Approx_frames.cache', 'tests/tests_pytest/GDS_Exact_frames.cache']
gstlal_channels = ['GDS-CALIB_STRAIN', 'GDS-CALIB_STRAIN']

# Set list of all channels
channel_list = []
act_channels = ['SUS-ETMX_L3_CAL_LINE_OUT_DQ', 'SUS-ETMX_L2_CAL_LINE_OUT_DQ', 'SUS-ETMX_L1_CAL_LINE_OUT_DQ']
for channel in act_channels:
        channel_list.append((ifo, channel))
for channel in gstlal_channels:
        channel_list.append((ifo, channel))

# Read stuff we need from the filters file
frequencies = []
act_corrections = []

act_line_names = ['ktst_esd', 'pum_act', 'uim_act']
EPICS = ['AT0_fT', 'AP0_fP', 'AU0_fU']
for i in range(len(act_line_names)):
        frequencies.append(float(filters["%s_line_freq" % act_line_names[i]]))
        act_corrections.append(float(filters["%s_re" % EPICS[i]]))
        act_corrections.append(float(filters["%s_im" % EPICS[i]]))

act_time_advance = 0.00006103515625
for i in range(0, len(act_corrections) // 2):
        corr = act_corrections[2 * i] + 1j * act_corrections[2 * i + 1]
        corr *= numpy.exp(2.0 * numpy.pi * 1j * frequencies[i] * act_time_advance)
        act_corrections[2 * i] = numpy.real(corr)
        act_corrections[2 * i + 1] = numpy.imag(corr)

demodulated_act_list = []
try:
        arm_length = float(filters['arm_length'])
except:
        arm_length = 3995.1

# demodulation and averaging parameters
filter_time = 20
average_time = 128
rate_out = 1

#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def act2darm(pipeline, name):

	# Get actuation injection channels from the raw frames
        act_inj_channels = []
        raw_data = pipeparts.mklalcachesrc(pipeline, location = "tests/tests_pytest/raw_frames.cache", cache_dsc_regex = ifo)
        raw_data = pipeparts.mkframecppchanneldemux(pipeline, raw_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
        for i in range(len(act_channels)):
                act_inj_channels.append(calibration_parts.hook_up(pipeline, raw_data, act_channels[i], ifo, 1.0))
                act_inj_channels[i] = calibration_parts.caps_and_progress(pipeline, act_inj_channels[i], "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "act_inj_%d" % i)
                act_inj_channels[i] = pipeparts.mktee(pipeline, act_inj_channels[i])

	# Demodulate the actuation injection channels at the lines of interest
        for i in range(len(frequencies)):
                demodulated_act = calibration_parts.demodulate(pipeline, act_inj_channels[i], frequencies[i], True, rate_out, filter_time, 0.5, prefactor_real = act_corrections[2 * i], prefactor_imag = act_corrections[2 * i + 1])
                demodulated_act_list.append(pipeparts.mktee(pipeline, demodulated_act))

        cache_num = 0
        for cache, channel, in zip(gstlal_frame_cache_list, gstlal_channels):
		# Get gstlal channels from the gstlal frames
                hoft_data = pipeparts.mklalcachesrc(pipeline, location = cache, cache_dsc_regex = ifo)
                hoft_data = pipeparts.mkframecppchanneldemux(pipeline, hoft_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
                hoft = calibration_parts.hook_up(pipeline, hoft_data, channel, ifo, 1.0, element_name_suffix = "_%d" % cache_num)
                hoft = calibration_parts.caps_and_progress(pipeline, hoft, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "%s_%d" % (channel, cache_num))
                deltal = pipeparts.mkaudioamplify(pipeline, hoft, arm_length)
                deltal = pipeparts.mktee(pipeline, deltal)

                for i in range(len(frequencies)):
			# Demodulate \DeltaL at each line
                        demodulated_deltal = calibration_parts.demodulate(pipeline, deltal, frequencies[i], True, rate_out, filter_time, 0.5)
		        # Take ratio \DeltaL(f) / act(f)
                        deltaL_over_act = calibration_parts.complex_division(pipeline, demodulated_deltal, demodulated_act_list[i])
			# Take a running average
                        deltaL_over_act = pipeparts.mkgeneric(pipeline, deltaL_over_act, "lal_smoothkappas", array_size = int(rate_out * average_time), no_default = True, filter_latency = 0.5)
			# The first samples are not averaged.  Remove only half, since sometimes early data is important.
                        deltaL_over_act = calibration_parts.mkinsertgap(pipeline, deltaL_over_act, insert_gap = False, chop_length = 500000000 * average_time)
			# Find the magnitude
                        deltaL_over_act = pipeparts.mktee(pipeline, deltaL_over_act)
                        magnitude = pipeparts.mkgeneric(pipeline, deltaL_over_act, "cabs")
			# Find the phase
                        phase = pipeparts.mkgeneric(pipeline, deltaL_over_act, "carg")
                        phase = pipeparts.mkaudioamplify(pipeline, phase, 180.0 / numpy.pi)
			# Interleave
                        magnitude_and_phase = calibration_parts.mkinterleave(pipeline, [magnitude, phase])
                        magnitude_and_phase = pipeparts.mkprogressreport(pipeline, magnitude_and_phase, name = "progress_sink_%s_%d_%d" % (channel, cache_num, i))
			# Write to file
                        pipeparts.mknxydumpsink(pipeline, magnitude_and_phase, "tests/tests_pytest/act_data/%s_%s_over_Act_%d_at_%0.1fHz.txt" % (ifo, channel.replace(' ', '_'), cache_num, frequencies[i]))
                cache_num = cache_num + 1

	#
	# done
	#

        return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#

#test_common.build_and_run(act2darm, "act2darm")
#class TestAct2darm:
#        """act2darm test class"""

#        def test_act2darm(self):
#                """Test act2darm"""
def Act2darm():
    test_common.build_and_run(act2darm, "act2darm", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))
    rms('A')
    rms('A', 'E')
    plot_act()

#test_common.build_and_run(act2darm, "act2darm", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))
#rms('A')
#rms('A', 'E')
#plot_act

#for i in range(len(gstlal_frame_cache_list)):
#	for j in range(len(frequencies)):
#	       standard_data = numpy.loadtxt("%s_%s_over_Act_standard_%d_at_%0.1fHz.txt" % (ifo, channel.replace(' ', '_'), cache_num, frequencies[i]))
#
#frequencies.sort()

#rms('A')
#rms('A', 'E')
#plot_pcal('A')
#plot_pcal('E')
