[InputConfigurations]
# Filters file containing calibration FIR filters
FiltersFileName: GDS_filters_PostO3_H1_20230315_testing.npz
# Data source should be set to frames or lvshm
DataSource: frames
FileChecksum: No
# Right now, SkipBadFiles needs to be off when reading from frames
SkipBadFiles: No
############################################
# If reading from frames use these options #
############################################
# None

###################################################
# If reading from shared memory use these options #
###################################################
SHMPartition: LHO_Online
# Assumed duration of input frames in seconds
InputFrameDuration: 1
# If there is no raw data coming in, we can begin to fill in missing data with zeros after this amount of time.  Set to 0.0 to disable.
WaitTime: 10.0

[OutputConfigurations]
CompressionScheme: 6
CompressionLevel: 3
ChanPrefix: GDS-
# Set to "None" if you do not want a channel suffix
ChanSuffix: None
# Data sink should be set to frames or lvshm
DataSink: frames
# Set this to write information about filters, configs, and gstlal-calibration version to the frame headers
# FIXME: We will probably do something different ut similar
# SetFrameHeader: Yes
#################################################
# If writing to shared memory use these options #
#################################################
OutputSHMPartition: LHO_hoft
BufferMode: 2
# Use this to approximate the frame size (in bytes) when writing to shared memory
FrameSize: 450000
NumBuffers: 10
###############################################
# If writing to frame files use these options #
###############################################
FrameType: H1GDS_Exact

[MonitoringConfigurations]
# This server is for LHO DMT
#KafkaServer: 192.168.1.2:9094
# This server is for LHO LDAS
#KafkaServer: 10.21.2.20:9182
#DataType: max
#InfluxHostname: influxdb.ligo.caltech.edu
#InfluxPort: 8086
#InfluxDatabaseName: gstlal_calibration
#AcrossJobs: Yes
#DumpPeriod: 1
#EnableAuth: No
#EnableHttps: No

[CalibrationConfigurations]
IFO: H1
# Set calibration mode to Full or Partial
CalibrationMode: Partial
ComputeCalibStateVector: Yes

[DebuggingConfigurations]
# If you want to write a pipeline graph, provide the graph name.  Otherwise, set name equal to None
PipelineGraphFilename: None
Verbose: Yes
# Turn this on to write data presentation timestamps and real-time unix timestamps to file at the beginning and end of the pipeline, to measure latency
TestLatency: No
# Turn this on to compute transfer functions for the filters by comparing output data to input data
TestFilters: No
# Turn this on to monitor the state vector
MonitorStatevector: No

[TDCFConfigurations]
#########################################################
# Options related to time dependent correction  factors #
#########################################################
# Set this to use an exact algebraic solution for the TDCFs, as described in P1900052, Section 5.2.6.
ComputeExactKappas: Yes

ComputeKappaTST: Yes
ApplyKappaTST: No
# Set this to have the \kappa_tst factors filter the actuation chain with an adaptive filter that corrects for both magnitude and phase errors.
ApplyComplexKappaTST: Yes

ComputeKappaPUM: Yes
ApplyKappaPUM: No
# Set this to have the \kappa_p factors the actuation chain with an adaptive filter that corrects for both magnitude and phase errors.
ApplyComplexKappaPUM: Yes

ComputeKappaUIM: Yes
ApplyKappaUIM: No
# Set this to have the \kappa_u factors the actuation chain with an adaptive filter that corrects for both magnitude and phase errors.
ApplyComplexKappaUIM: Yes

ComputeKappaC: Yes
ApplyKappaC: Yes

ComputeFcc: Yes
ApplyFcc: Yes

ComputeSRCQ: Yes
ApplySRCQ: Yes

ComputeFs: Yes
ApplyFs: Yes

###########################################
# Options related to the coherence gating #
###########################################
UseCoherence: Yes
CoherenceUncThreshold: 0.01
# Amount of time used in front-end to compute coherence
CoherenceTime: 130
###################################################################
# Options related to the computation configurations for the TDCFs #
###################################################################
# Length in seconds of low-pass FIR filter used in demodulation of the calibration lines
DemodulationFilterTime: 32
# Time (in seconds) to smooth out \kappas with a running median
MedianSmoothingTime: 128
TDCFAveragingTime: 10
#If set to yes, bad computed kappas will be replaced by the previous computed median in the running median array. Otherwise, they are replaced with the default value
TDCFDefaultToMedian: Yes
# If using X-end Pcal, we need a minus sign, so set this to -1.0
PcalSign: 1.0
############################
# Nominal values for TDCFs #
############################
ExpectedKappaTSTReal: 1.0
ExpectedKappaTSTImag: 0.0
ExpectedKappaPUMReal: 1.0
ExpectedKappaPUMImag: 0.0
ExpectedKappaUIMReal: 1.0
ExpectedKappaUIMImag: 0.0
ExpectedKappaC: 1.0
################################
# Acceptable variance in TDCFs #
################################
KappaTSTRealVar: 0.2
KappaTSTImagVar: 0.2
KappaPUMRealVar: 0.2
KappaPUMImagVar: 0.2
KappaUIMRealVar: 0.2
KappaUIMImagVar: 0.2
KappaCVar: 0.2
FccVar: 50.0
FsVar: 5.0
SRCQInvMin: 0.0
SRCQInvMax: 0.5
#######################
# EPICS records input #
#######################
# Set to Yes if EPICS records for TDCF computations should be read from filters file.  If set to No, they will be read from frames
FactorsFromFiltersFile: Yes
##############################################################################################
# Updating Sensing and Actuation filters with all frequency-dependent corrections parameters #
##############################################################################################
# Length of time (in seconds) between when inverse-sensing FIR filter is updated
SensingFilterUpdateTime: 64
# Length of time (in seconds) over which the smoothed time-dependent parameters of the sensing function are averaged before updating the filter
SensingFilterAveragingTime: 1
# Number of samples to be used when tapering old inverse sensing filter and ramping in new filter
SensingFilterTaperLength: 32768
# Set to Yes to apply the time-dependent portion of the sensing function as a 1+N_z tap filter, where N_z is the number of time-varying zeros in the inverse sensing function
MinimizeAdaptiveSensingFilter: No
# Length of time (in seconds) between when the actuation FIR filters are updated
ActuationFilterUpdateTime: 64
# Length of time (in seconds) over which the smoothed time-dependent parameters of the actuation function are averaged before updating the filter
ActuationFilterAveragingTime: 1
# Number of samples to be used when tapering old actuation filters and ramping in new filters
ActuationFilterTaperLength: 32768

[ChannelNames]
#############################
# Calibration Channel Names #
#############################
DARMCtrlChannel: CAL-DARM_CTRL_DBL_DQ
DARMErrChannel: CAL-DARM_ERR_DBL_DQ
DeltaLTSTChannel: CAL-DELTAL_CTRL_TST_DBL_DQ
DeltaLPUMChannel: CAL-DELTAL_CTRL_PUM_DBL_DQ
DeltaLUIMChannel: CAL-DELTAL_CTRL_UIM_DBL_DQ
DeltaLResChannel: CAL-DELTAL_RESIDUAL_DBL_DQ
####################################
# Data Quality Vector Channel Name #
####################################
LowNoiseStateChannel: GRD-IFO_READY
HWInjChannel: CAL-INJ_STATUS_OUT_DQ
ObsIntentChannel: GRD-IFO_INTENT
FilterClockChannelList: GRD-ISC_LOCK_OK
#UndisturbedOKChannel: None
##################################
# Calibration Line Channel Names #
##################################
TSTExcChannel: SUS-ETMX_L3_CAL_LINE_OUT_DQ
PUMExcChannel: SUS-ETMX_L2_CAL_LINE_OUT_DQ 
UIMExcChannel: SUS-ETMX_L1_CAL_LINE_OUT_DQ
PCALChannel: CAL-DELTAL_REF_PCAL_DQ
############################################
# Calibration Line Frequency Channel Names #
############################################
#TSTExcLineFreqChannel: CAL-CS_TDEP_SUS_LINE3_COMPARISON_OSC_FREQ
#PUMExcLineFreqChannel: CAL-CS_TDEP_SUS_LINE2_COMPARISON_OSC_FREQ
#UIMExcLineFreqChannel: CAL-CS_TDEP_SUS_LINE1_COMPARISON_OSC_FREQ
TSTExcLineFreqChannel: SUS-ETMX_L3_CAL_LINE_FREQ
PUMExcLineFreqChannel: SUS-ETMX_L2_CAL_LINE_FREQ
UIMExcLineFreqChannel: SUS-ETMX_L1_CAL_LINE_FREQ
PcalLine1FreqChannel: CAL-PCALY_PCALOSC1_OSC_FREQ
PcalLine1CorrRealChannel: CAL-CS_TDEP_PCAL_LINE1_CORRECTION_REAL
PcalLine1CorrImagChannel: CAL-CS_TDEP_PCAL_LINE1_CORRECTION_IMAG
PcalLine2FreqChannel: CAL-PCALY_PCALOSC2_OSC_FREQ
PcalLine2CorrRealChannel: CAL-CS_TDEP_PCAL_LINE2_CORRECTION_REAL
PcalLine2CorrImagChannel: CAL-CS_TDEP_PCAL_LINE2_CORRECTION_IMAG
PcalLine3FreqChannel: CAL-PCALY_PCALOSC3_OSC_FREQ
PcalLine3CorrRealChannel: CAL-CS_TDEP_PCAL_LINE3_CORRECTION_REAL
PcalLine3CorrImagChannel: CAL-CS_TDEP_PCAL_LINE3_CORRECTION_IMAG

#######################################
# Coherence Uncertainty Channel Names #
#######################################
CohUncSusLine1Channel: CAL-CS_TDEP_SUS_LINE1_UNCERTAINTY
CohUncSusLine2Channel: CAL-CS_TDEP_SUS_LINE2_UNCERTAINTY
CohUncSusLine3Channel: CAL-CS_TDEP_SUS_LINE3_UNCERTAINTY
CohUncPcalyLine1Channel: CAL-CS_TDEP_PCAL_LINE1_UNCERTAINTY
CohUncPcalyLine2Channel: CAL-CS_TDEP_PCAL_LINE2_UNCERTAINTY
CohUncPcalyLine4Channel: CAL-CS_TDEP_PCAL_LINE4_UNCERTAINTY
CohUncDARMLine1Channel: CAL-CS_TDEP_DARM_LINE1_UNCERTAINTY
###################################
# Noise Subtraction Channel Names #
###################################
# Semicolon-separated list of comma-separated lists of witness channels to use to subtract lines from h(t)
# Semicolons separate channels to be handled separately and commas separate channels to be handled together.
#Set to None if no line witness channels are to be used
LineWitnessChannelList: None
#SUS-ETMX_L1_CAL_LINE_OUT_DQ;SUS-ETMX_L2_CAL_LINE_OUT_DQ;SUS-ETMX_L3_CAL_LINE_OUT_DQ;CAL-PCALY_RX_PD_OUT_DQ;CAL-PCALX_RX_PD_OUT_DQ;PEM-EY_MAINSMON_EBAY_1_DQ
# List of channels with frequencies of the lines in the line witness channels, with the same format and dimensions as LineWitnessFreqs.  To not use a frequency channel, use "None."  To indicate a harmonic or multiple of a previous frequency, use a float, like 2.0.
LineWitnessFrequencyChannelList: SUS-ETMX_L1_CAL_LINE_FREQ;SUS-ETMX_L2_CAL_LINE_FREQ;SUS-ETMX_L3_CAL_LINE_FREQ;CAL-PCALY_PCALOSC1_OSC_FREQ,CAL-PCALY_PCALOSC2_OSC_FREQ,CAL-PCALY_PCALOSC3_OSC_FREQ,CAL-PCALY_PCALOSC4_OSC_FREQ,CAL-PCALY_PCALOSC5_OSC_FREQ,CAL-PCALY_PCALOSC6_OSC_FREQ,CAL-PCALY_PCALOSC7_OSC_FREQ,CAL-PCALY_PCALOSC8_OSC_FREQ,CAL-PCALY_PCALOSC9_OSC_FREQ;CAL-PCALX_PCALOSC1_OSC_FREQ,CAL-PCALX_PCALOSC2_OSC_FREQ,CAL-PCALX_PCALOSC3_OSC_FREQ,CAL-PCALX_PCALOSC4_OSC_FREQ,CAL-PCALX_PCALOSC5_OSC_FREQ,CAL-PCALX_PCALOSC6_OSC_FREQ,CAL-PCALX_PCALOSC7_OSC_FREQ,CAL-PCALX_PCALOSC8_OSC_FREQ,CAL-PCALX_PCALOSC9_OSC_FREQ;None,None,None,None,None
# Semicolon-separated list of comma-separated lists of amplitudes, 0 or 1, indicating when the above lines are on.
LineAmplitudeList: SUS-ETMX_L1_CAL_LINE_CLKGAIN;SUS-ETMX_L2_CAL_LINE_CLKGAIN;SUS-ETMX_L3_CAL_LINE_CLKGAIN;CAL-PCALY_PCALOSC1_OSC_SINGAIN,CAL-PCALY_PCALOSC2_OSC_SINGAIN,CAL-PCALY_PCALOSC3_OSC_SINGAIN,CAL-PCALY_PCALOSC4_OSC_SINGAIN,CAL-PCALY_PCALOSC5_OSC_SINGAIN,CAL-PCALY_PCALOSC6_OSC_SINGAIN,CAL-PCALY_PCALOSC7_OSC_SINGAIN,CAL-PCALY_PCALOSC8_OSC_SINGAIN,CAL-PCALY_PCALOSC9_OSC_SINGAIN;CAL-PCALX_PCALOSC1_OSC_SINGAIN,CAL-PCALX_PCALOSC2_OSC_SINGAIN,CAL-PCALX_PCALOSC3_OSC_SINGAIN,CAL-PCALX_PCALOSC4_OSC_SINGAIN,CAL-PCALX_PCALOSC5_OSC_SINGAIN,CAL-PCALX_PCALOSC6_OSC_SINGAIN,CAL-PCALX_PCALOSC7_OSC_SINGAIN,CAL-PCALX_PCALOSC8_OSC_SINGAIN,CAL-PCALX_PCALOSC9_OSC_SINGAIN;None,None,None,None,None
# Semicolon-separated list of comma-separated lists of witness channels to use to subtract noise from h(t)
# Set to None if no witness channels are to be used
#WitnessChannelList: PSL-ISS_SECONDLOOP_OUTPUT_DQ,ASC-DHARD_P_OUT_DQ,ASC-DHARD_Y_OUT_DQ,ASC-CHARD_P_OUT_DQ,ASC-CHARD_Y_OUT_DQ,LSC-SRCL_IN1_DQ,LSC-MICH_IN1_DQ,LSC-PRCL_IN1_DQ
WitnessChannelList: None
# What channel should we use to gate the noise subtraction and 60-Hz line subtraction
NoiseSubGateChannel: GRD-ISC_LOCK_STATE_N
####################################
# Nonsens Subtraction Channel Name #
####################################
NonsensSubtractionChannel: None
#OAF-CLEANING_NOISE_EST_DBL_DQ
###############################
# EPICS Records Channel Names #
###############################
EP1RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_INVA_TST_RESPRATIO_REAL
EP1ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_INVA_TST_RESPRATIO_IMAG
EP2RealChannel: CAL-CS_TDEP_REF_CLGRATIO_CTRL_REAL
EP2ImagChannel: CAL-CS_TDEP_REF_CLGRATIO_CTRL_IMAG
EP3RealChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_INV_REAL
EP3ImagChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_INV_IMAG
EP4RealChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_TST_REAL
EP4ImagChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_TST_IMAG
EP5RealChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_REAL
EP5ImagChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_IMAG
EP6RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NOCAVPOLE_REAL
EP6ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NOCAVPOLE_IMAG
EP7RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_D_REAL
EP7ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_D_IMAG
EP8RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_A_TST_REAL
EP8Imagchannel: CAL-CS_TDEP_PCAL_LINE2_REF_A_TST_IMAG
EP9RealChannel: CAL-CS_TDEP_PCALY_LINE2_REF_A_USUM_REAL
EP9ImagChannel: CAL-CS_TDEP_PCALY_LINE2_REF_A_USUM_IMAG
EP10RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NL_REAL
EP10ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NL_IMAG
EP11RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NOCAVPOLE_REAL
EP11ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NOCAVPOLE_IMAG
EP12RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_D_REAL
EP12ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_D_IMAG
EP13RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_A_TST_REAL
EP13ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_A_TST_IMAG
EP14RealChannel: CAL-CS_TDEP_PCALY_LINE4_REF_A_USUM_REAL
EP14ImagChannel: CAL-CS_TDEP_PCALY_LINE4_REF_A_USUM_IMAG
EP15RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_INVA_PUM_RESPRATIO_REAL
EP15Imagchannel: CAL-CS_TDEP_SUS_LINE2_REF_INVA_PUM_RESPRATIO_IMAG
EP16RealChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_UIM_INV_REAL
EP16ImagChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_UIM_INV_IMAG
EP17RealChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_PUM_REAL
EP17ImagChannel: CAL-CS_TDEP_DARM_LINE1_REF_A_PUM_IMAG
EP18RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_A_PUM_REAL
EP18ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_A_PUM_IMAG
EP19RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_A_UIM_REAL
EP19ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_A_UIM_IMAG
EP20RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_A_PUM_REAL
EP20ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_A_PUM_IMAG
EP21RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_A_UIM_REAL
EP21ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_A_UIM_IMAG
EP22RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_INVA_UIM_RESPRATIO_REAL
EP22ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_INVA_UIM_RESPRATIO_IMAG
EP23RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NL_REAL
EP23ImagChannel: CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NL_IMAG
EP24RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NL_REAL
EP24ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NL_IMAG
EP25RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_TST_REAL
EP25ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_TST_IMAG
EP26RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_PUM_REAL
EP26ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_PUM_IMAG
EP27RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_UIM_REAL
EP27ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_UIM_IMAG
EP28RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_TST_REAL
EP28ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_TST_IMAG
EP29RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_PUM_REAL
EP29ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_PUM_IMAG
EP30RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_UIM_REAL
EP30ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_UIM_IMAG
EP31RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_A_TST_NL_REAL
EP31ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_A_TST_NL_IMAG
EP32RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_TST_REAL
EP32ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_TST_IMAG
EP33RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_PUM_REAL
EP33ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_PUM_IMAG
EP34RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_UIM_REAL
EP34ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_UIM_IMAG
EP35RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_A_PUM_NL_REAL
EP35ImagChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_A_PUM_NL_IMAG
EP36RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_TST_REAL
EP36ImagChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_TST_IMAG
EP37RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_PUM_REAL
EP37ImagChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_PUM_IMAG
EP38RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_UIM_REAL
EP38ImagChannel: CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_UIM_IMAG
EP39RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_A_UIM_NL_REAL
EP39ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_A_UIM_NL_IMAG
EP40RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_TST_REAL
EP40ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_TST_IMAG
EP41RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_PUM_REAL
EP41ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_PUM_IMAG
EP42RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_UIM_REAL
EP42ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_UIM_IMAG
EP43RealChannel: CAL-CS_TDEP_PCAL_LINE1_REF_RESP_REAL
EP43ImagChannel: CAL-CS_TDEP_PCAL_LINE1_REF_RESP_IMAG
EP44RealChannel: CAL-CS_TDEP_PCAL_LINE2_REF_RESP_REAL
EP44ImagChannel: CAL-CS_TDEP_PCAL_LINE2_REF_RESP_IMAG
EP45RealChannel: CAL-CS_TDEP_SUS_LINE3_REF_RESP_OVER_A_TST_NL_REAL
EP45ImagChannel: CAL-CS_TDEP_SUS_LINE3_REF_RESP_OVER_A_TST_NL_IMAG
EP46RealChannel: CAL-CS_TDEP_SUS_LINE2_REF_RESP_OVER_A_PUM_NL_REAL
EP46ImagChannel: CAL-CS_TDEP_SUS_LINE2_REF_RESP_OVER_A_PUM_NL_IMAG
EP47RealChannel: CAL-CS_TDEP_SUS_LINE1_REF_RESP_OVER_A_UIM_NL_REAL
EP47ImagChannel: CAL-CS_TDEP_SUS_LINE1_REF_RESP_OVER_A_UIM_NL_IMAG

[SampleRates]
# Sample rate at which to compute h(t)
HoftSR: 16384
# Sample rate at which to compute CALIB_STATE_VECTOR
CalibStateSR: 16
# Sample rate of control channel
# Should be 16384 if using DARM_CTRL and 4096 if using DELTAL_CTRL 
CtrlSR: 4096
# Sample rate of low noise state channel
LowNoiseSR: 16
# Sample rate of HW injection channel
HWInjSR: 16384
# Sample rate of observation intent channel
ObsIntentSR: 16
# Sample rate list for channels being used to trigger filter settling clock
FilterClockSRList: 16
# Sample rate of the undisturbed ok channel
UndisturbedOKSR: 16
# Sample rate of TST excitation channel
TSTExcSR: 512
# Sample rate of PUM excitation channel
PUMExcSR: 512
# Sample rate of UIM excitation channel
UIMExcSR: 512
# Sample rate of coherence channels
CohSR: 16
# Sample rate for the EPICS reference channels
EPICSRefSR: 16
# Sample rates at which transfer functions will be computed and witness channels will be filtered, given as a semicolon-separated list, e.g., 2048;2048;512;2048. This must be given if WitnessChannelList is not None, and it must be the same length.
WitnessChannelSR: 512
# Sample rates at which to compute and record TDCFs
ComputeFactorsSR: 16
RecordFactorsSR: 16
# Sample rate of the NonSENS subtraction channel
NonsensSubtractionSR: 16384

[Bitmasks]
ObsReadyBitmask: 1
ObsIntentBitmask: 1
CBCHWInjOffBitmask: 9
BurstHWInjOffBitmask: 17
DetCharHWInjOffBitmask: 33
StochHWInjOffBitmask: 65
NoiseSubGateBitmask: 512
# If we require NoiseSubGateChannel > some value, use this instead.  Otherwise, set to "None"
NoiseSubGateThreshold: 599
FilterClockBitmaskList: 1
UndisturbedOKBitmask: 1

[PipelineConfigurations]
BufferLength: 1.0
FrequencyDomainFiltering: No
Dewhitening: No
# Latency of all filtering/averaging/median processes (other than calibration model filters) as a fraction of filter length. Value should be set between 0.0 and 1.0.
FilterLatency: 0.0

[DataCleaningConfigurations]
###################################################
# Options for turning on and off line subtraction #
###################################################
# Remove the DC component from the residual and control channels before filtering
RemoveDC: No
###############################################################
# Options for running line subtraction using witness channels #
###############################################################
# Semicolon-separated list of comma-separated lists of frequencies (same number of semicolons as LineWitnessChannelList)
LineWitnessFreqs: 15.6;16.4;17.6;17.1,410.3,1083.7,700.1,701.1,702.1,703.1,704.1,1153.2;3501.3,410.2,1083.3,33.43,56.39,77.73,102.13,283.91,1153.1;60.0,120.0,180.0,240.0,300.0
# Length of the filter used to demodulate lines being subtracted
LineSubtractionFilterTime: 32
# Time over which to take a median of transfer functions
LineWitnessTFMedianTime: 512
# Time over which to average transfer functions
LineWitnessTFAveragingTime: 1
# Semicolon-separated list of typical frequency fluctuations in lines being subtracted (same number of semicolons as LineWitnessChannelList)
LineWitnessFreqVars: 0.0;0.0;0.0;0.0;0.0;0.02
#######################################
# Options for broadband noise removal #
#######################################
# The length in seconds of the fast Fourier transforms used to compute transfer functions between witness channels and h(t). The fft's are windowed with Hann windows and overlapped.
WitnessChannelFFTTime: 4.0
# The number of ffts to take before averaging the witness -> h(t) transfer functions calculation. The average is taken after the ratio h(f) / witness(f).
NumWitnessFFTs: 1000
# Sets the minimum number of FFTs necessary to produce the first transfer functions and clean data after data flow starts.
MinWitnessFFTs: 300
# The length in seconds of the filters applied to the witness channels before subtracting from h(t)
WitnessFIRLength: 1.0
# The frequency resolution of the filters applied to the witness channels before subtracting from h(t). It can be advantageous to lower the frequency resolution in order to average over excess noise.
WitnessFrequencyResolution: 0.5
# List of minima and maxima of frequency ranges where the Fourier transform of h(t) will be replaced by a straight line in the calculation of transfer functions between witness channels and h(t) for noise subtraction. Semicolons separate lists for different sets of witness channels. If no notches are desired, use zeros, e.g., \'0;0;0\'. Here is an example using the expected format: \'495.0,515.0,985.0,1015.0;59,60,119,121;0\' This can be useful, e.g., if there are loud lines in the signal that are not present in the witness channels.
WitnessNotchFrequencies: 0
# Cutoff frequencies for high-pass filters for witness channels
WitnessHighPasses: 10
# The amount of time after transfer functions between witness channels and h(t) are finished to begin the calculation of the next set of transfer functions
WitnessTFUpdateTime: 4
# If lock-loss lasts at least this many seconds, transfer functions will revert to those computed at the beginning of a lock stretch. Set to zero to disbale.
CriticalLockLossTime: 1800
# The amount of time to use to taper in newly computed FIR filters for witness channels being used for noise subtraction.
WitnessFilterTaperTime: 2
# If writing transfer functions to file, this sets the name. If transfer functions should not be written to file, this should be set to None
WitnessTFFilename: None
# Should the transfer function calculation use a median? If not, an average (mean) is used.
WitnessTFUseMedian: Yes
# Should transfer functions be computed on a fixed schedule, so that the output does not depend on start time?  This is useful for running jobs in parallel.  Otherwise, they are computed asap.
WitnessTFParallelMode: No
# When using parallel mode, how many seconds later should we shift the time when transfer functions start being computed from a multiple of the cycle period?
WitnessTFTimeShift: 360
# To write only one strain channel, and pick whichever is less noisy, set this option to 'Yes'. This way, if a problem arises with the noise subtraction it will not affect the output strain channel.
PickCleanestStrainChannel: No
# A half Hann window is used for transitions between cleaned and uncleaned data.  This is the length of that window in seconds.
StrainChannelTransitionTime: 10.0
###############################
# Options for HOFT_CLEAN bits #
###############################
# The amount of data from h(t) and cleaned h(t) that is used to compute and compare the rms. This comparison between cleaned and uncleaned h(t) determines whether the HOFT_CLEAN bits of the calibration state vector are on or off.
CleaningCheckRMSTime: 128.0
# Minimum of a range of frequencies in which we expect line/noise subtraction to be impactful. The HOFT_CLEAN_LOWFREQ_OK bit of the calibration state vector is determined based on whether rms of the cleaned data is less than that of uncleaned h(t) in this range.
CleaningCheckRangeLowMin: 15
# Maximum of a range of frequencies in which we expect line/noise subtraction to be impactful. The HOFT_CLEAN_LOWFREQ_OK bit of the calibration state vector is determined based on whether rms of the cleaned data is less than that of uncleaned h(t) in this range.
CleaningCheckRangeLowMax: 20
# Minimum of a range of frequencies in which we expect line/noise subtraction to be impactful. The HOFT_CLEAN_MIDFREQ_OK bit of the calibration state vector is determined based on whether rms of the cleaned data is less than that of uncleaned h(t) in this range.
CleaningCheckRangeMidMin: 20
# Maximum of a range of frequencies in which we expect line/noise subtraction to be impactful. The HOFT_CLEAN_MIDFREQ_OK bit of the calibration state vector is determined based on whether rms of the cleaned data is less than that of uncleaned h(t) in this range.
CleaningCheckRangeMidMax: 450
# Minimum and Maximum of a frequency band in which we expect NonSENS subtraction to be impactful.  The NONSENS_SUBTRACTION_OK bit of the calibration state vector is determined based on whether rms of the data is reduced in this range.
NonsensCheckRangeMin: 20
NonsensCheckRangeMax: 100
