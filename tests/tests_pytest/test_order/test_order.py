import subprocess
import shlex
import os
from tests.tests_pytest.State_Vector import State_Vector
from tests.tests_pytest.ASD import ASD
from tests.tests_pytest.act2darm_timeseries import Act2darm
from tests.tests_pytest.pcal2darm_timeseries import Pcal2darm
from tests.tests_pytest.run_calib_pipeline import Run_calib
from tests.tests_pytest.diff import Diff
from tests.tests_pytest.latency import Latency


def test_Calib_Pipeline():
    Run_calib()

"""
def test_ASD():
    ASD()

def test_state_vector():
    State_Vector()

def test_act2darm():
    Act2darm()

def test_pcal2darm():
    Pcal2darm()

def test_diff():
    Diff()

def test_latency():
    Latency()
"""

