import os

def test_gstlal_compute_strain_running():
    os.system("rm -rf tests/tests_pytest/frames/hoft/test_pipeline/")
    os.system("mkdir -p tests/tests_pytest/frames/hoft/test_pipeline/")
    os.system("ls tests/tests_pytest/frames/raw/*.gwf | lal_path2cache > tests/tests_pytest/test_raw_frames.cache")
    os.system("gstlal_compute_strain --config-file tests/tests_pytest/configs/gstlal_compute_strain_test_H1.ini --filters-file tests/tests_pytest/filters/gstlal_compute_strain_C00_filters_H1_20230613_ci.npz --gps-start-time 1370673024 --gps-end-time 1370673792 --frame-cache tests/tests_pytest/test_raw_frames.cache --wings=256 --output-path tests/tests_pytest/frames/hoft/test_pipeline/")
    num_files = len(os.listdir('tests/tests_pytest/frames/hoft/test_pipeline/'))
    assert num_files == 64

