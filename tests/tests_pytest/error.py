#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets, Alexander Bartoletti
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import numpy as np

#def rms(hoft_f='hoft_asd.txt', clean_f='clean_asd.txt'): 

def rms(d_type, typ='A'):
    pcal_freq_list = [102.1, 1083.3, 1083.7, 17.1, 283.9, 33.4, 410.2, 410.3, 56.4, 77.7]
    act_freq_list = [15.6, 16.4, 17.6]
    st_f_list = []
    f_list = []
    if d_type[0].upper() == 'H':
            name = 'hoft'
            st_f_list.append('tests/tests_pytest/ASD_data/hoft_asd_standard.txt')
            f_list.append('tests/tests_pytest/ASD_data/hoft_asd.txt')
    elif d_type[0].upper() == 'C':
            name = 'clean'
            st_f_list.append('tests/tests_pytest/ASD_data/clean_asd_standard.txt')
            f_list.append('tests/tests_pytest/ASD_data/clean_asd.txt')
    elif d_type[0].upper() == 'P':
            #typ = input('Approx or Exact: ')
            if typ[0].upper() == 'A':
                    name = 'Approx pcal'
                    for freq in pcal_freq_list:
                            f_list.append('tests/tests_pytest/pcal_data/H1_GDS-CALIB_STRAIN_over_Pcal_0_at_%sHz.txt' % freq)
                            st_f_list.append('tests/tests_pytest/pcal_data/H1_GDS-CALIB_STRAIN_over_Pcal_standard_0_at_%sHz.txt' % freq)
            if typ[0].upper() == 'E':
                    name = 'Exact pcal'
                    for freq in pcal_freq_list:
                            f_list.append('tests/tests_pytest/pcal_data/H1_GDS-CALIB_STRAIN_over_Pcal_1_at_%sHz.txt' % freq)
                            st_f_list.append('tests/tests_pytest/pcal_data/H1_GDS-CALIB_STRAIN_over_Pcal_standard_1_at_%sHz.txt' % freq)
    elif d_type[0].upper() == 'A':
            if typ[0].upper() == 'A':
                    name = 'Approx act'
                    for freq in act_freq_list:
                            f_list.append('tests/tests_pytest/act_data/H1_GDS-CALIB_STRAIN_over_Act_0_at_%sHz.txt' % freq)
                            st_f_list.append('tests/tests_pytest/act_data/H1_GDS-CALIB_STRAIN_over_Act_standard_0_at_%sHz.txt' % freq)
            if typ[0].upper() == 'E':
                    name = 'Exact act'
                    for freq in act_freq_list:
                            f_list.append('tests/tests_pytest/act_data/H1_GDS-CALIB_STRAIN_over_Act_1_at_%sHz.txt' % freq)
                            st_f_list.append('tests/tests_pytest/act_data/H1_GDS-CALIB_STRAIN_over_Act_standard_1_at_%sHz.txt' % freq)


    e_file = open('tests/tests_pytest/error_results.txt', 'a')
    for f in st_f_list:
            standard = np.loadtxt(f)
    for f in f_list:
            data = np.loadtxt(f)
    standard = np.transpose(standard)[-1]
    data = np.transpose(data)[-1]
    sum_sq = 0
    for i in range(len(standard-3)):
            if standard[i] != 0: 
                    sum_sq += abs(1 - data[i]/standard[i])
    mean_sq = sum_sq/len(data)
    rms = np.sqrt(mean_sq)
    #try:
    #assert rms < 1
    #except AssertionError:
    #s1 = 'bad ' + str(name) + ' data' + '\n'
    #e_file.write(s1)
    #finally:
    s2 = str(name) + ' rms = ' + str(rms) + '\n'
    e_file.write(s2)
    e_file.close()
    assert rms < 1
#error between 1% and 10^-15
