#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import sys
import os
import numpy
import time
import resource

from optparse import OptionParser, Option

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlalcalibration import test_common

parser = OptionParser()
parser.add_option("--ifo", metavar = "name", help = "Name of interferometer (either L1 or H1)")
parser.add_option("--gps-start-time", metavar = "seconds", type = int, help = "GPS time when to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, help = "GPS time when to stop processing data")
parser.add_option("--frame-cache", metavar = "name", help = "Frame cache file with input data")
parser.add_option("--frame-type", metavar = "name", help = "Type of frame file, e.g., H1_R or L1_R for raw frames, H1_HOFT_C00 or L1_HOFT_C00 for low-latency production frames, etc.")
parser.add_option("--channel-prefix", metavar = "name", default = "", help = "Prefix for channels to read, also given to output channels (default is no prefix)")
parser.add_option("--channel-suffix", metavar = "name", default = "", help = "Suffix for channels to read, also given to output channels (default is no suffix)")
parser.add_option("--frame-length", metavar = "seconds", type = int, default = 64, help = "Length of output frames in seconds (Default = 64)")
parser.add_option("--frames-per-file", type = int, default = 1, help = "Number of frames per frame file (Default = 1)")
parser.add_option("--output-path", metavar = "name", default = '.', help = "Location where to write frames.  If string contains '.txt', data will be written to txt files.  Otherwise, channels are written to frames.")
parser.add_option("--channel-list", metavar = "name", default = None, help = "Comma-separated list of channels to read from frames")
parser.add_option("--filters-file", metavar = "name", default = None, help = "Name of a filters file containing EPICS records to write to channels")

options, filenames = parser.parse_args()

#
# 
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


#
# This pipeline reads the channels needed for calibration from the raw frames
# and writes them to smaller frames for faster access. It can also change the
# frame length if desired
#

ifo = options.ifo
frame_cache = options.frame_cache
frame_type = options.frame_type
output_path = options.output_path
frame_length = options.frame_length
frames_per_file = options.frames_per_file
chan_prefix = options.channel_prefix
chan_suffix = options.channel_suffix
channel_list = [] if options.channel_list is None else options.channel_list.split(',')

if not any(channel_list):
	# Get a list of available channels from frchannels.txt, a file which should be written before running this script
	# FIXME: I'm sure there's a better way to do this. It would be nice to check the first frame file in the cache using Python alone.
	try:
		f = open('frchannels.txt', 'r')
		available_channels = f.read()
		f.close()
		print("Making channel list using available channels")
	except:
		print("Cannot find file frchannels.txt. Run FrChannels to check for available channels")

	# These are (or may be) in the calibrated frames
	channel_list.append(chan_prefix + "CALIB_STRAIN" + chan_suffix)
	channel_list.append("ODC-MASTER_CHANNEL_OUT_DQ")
	channel_list.append(chan_prefix + "CALIB_STATE_VECTOR" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_STRAIN_CLEAN" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_TST_REAL" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_TST_IMAGINARY" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_TST_REAL_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_TST_IMAGINARY_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_A_REAL" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_A_IMAGINARY" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PU_REAL" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PU_IMAGINARY" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PU_REAL_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PU_IMAGINARY_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PUM_REAL" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PUM_IMAGINARY" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PUM_REAL_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_PUM_IMAGINARY_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_UIM_REAL" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_UIM_IMAGINARY" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_UIM_REAL_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_UIM_IMAGINARY_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_C" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_KAPPA_C_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_F_CC" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_F_CC_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_F_S" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_SRC_Q_INVERSE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_F_S_NOGATE" + chan_suffix)
	channel_list.append(chan_prefix + "CALIB_SRC_Q_INVERSE_NOGATE" + chan_suffix)

	# These are (or may be) in the raw frames
	channel_list.append("OAF-CLEANING_NOISE_EST_DBL_DQ")
	channel_list.append("CAL-INJ_STATUS_OUT_DQ")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE1_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE3_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_UNCERTAINTY")
	channel_list.append("CAL-CS_LINE_SUM_DQ")
	channel_list.append("CAL-CS_SUS_L3_CAL_LINE_OUT_DQ")
	channel_list.append("CAL-CS_SUS_L2_CAL_LINE_OUT_DQ")
	channel_list.append("CAL-CS_SUS_L1_CAL_LINE_OUT_DQ")
	channel_list.append("CAL-DELTAL_REF_PCAL_DQ")
	channel_list.append("SUS-ETMY_L1_CAL_LINE_OUT_DQ")
	channel_list.append("SUS-ETMY_L2_CAL_LINE_OUT_DQ")
	channel_list.append("SUS-ETMY_L3_CAL_LINE_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_CAL_LINE_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_DRIVEALIGN_L_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_DRIVEALIGN_P_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_DRIVEALIGN_Y_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_MASTER_OUT_LL_DQ")
	channel_list.append("SUS-ETMX_L1_MASTER_OUT_LR_DQ")
	channel_list.append("SUS-ETMX_L1_MASTER_OUT_UL_DQ")
	channel_list.append("SUS-ETMX_L1_MASTER_OUT_UR_DQ")
	channel_list.append("SUS-ETMX_L1_OLDAMP_P_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_OLDAMP_Y_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_OSEMINF_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_OSEMINF_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_OSEMINF_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_OSEMINF_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_WIT_L_DQ")
	channel_list.append("SUS-ETMX_L1_WIT_P_DQ")
	channel_list.append("SUS-ETMX_L1_WIT_Y_DQ")
	channel_list.append("SUS-ETMX_L1_FASTIMON_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_FASTIMON_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_FASTIMON_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_FASTIMON_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_NOISEMON_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_NOISEMON_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_NOISEMON_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L1_NOISEMON_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_CAL_LINE_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_DRIVEALIGN_L_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_DRIVEALIGN_P_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_DRIVEALIGN_Y_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_MASTER_OUT_LL_DQ")
	channel_list.append("SUS-ETMX_L2_MASTER_OUT_LR_DQ")
	channel_list.append("SUS-ETMX_L2_MASTER_OUT_UL_DQ")
	channel_list.append("SUS-ETMX_L2_MASTER_OUT_UR_DQ")
	channel_list.append("SUS-ETMX_L2_OLDAMP_P_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_OLDAMP_Y_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_OSEMINF_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_OSEMINF_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_OSEMINF_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_OSEMINF_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_WIT_L_DQ")
	channel_list.append("SUS-ETMX_L2_WIT_P_DQ")
	channel_list.append("SUS-ETMX_L2_WIT_Y_DQ")
	channel_list.append("SUS-ETMX_L2_FASTIMON_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_FASTIMON_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_FASTIMON_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_FASTIMON_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_NOISEMON_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_NOISEMON_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_NOISEMON_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L2_NOISEMON_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_CAL_LINE_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_DRIVEALIGN_L_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_DRIVEALIGN_P_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_DRIVEALIGN_Y_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_MASTER_OUT_LL_DQ")
	channel_list.append("SUS-ETMX_L3_MASTER_OUT_LR_DQ")
	channel_list.append("SUS-ETMX_L3_MASTER_OUT_UL_DQ")
	channel_list.append("SUS-ETMX_L3_MASTER_OUT_UR_DQ")
	channel_list.append("SUS-ETMX_L3_OLDAMP_P_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_OLDAMP_Y_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_OSEMINF_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_OSEMINF_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_OSEMINF_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_OSEMINF_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_WIT_L_DQ")
	channel_list.append("SUS-ETMX_L3_WIT_P_DQ")
	channel_list.append("SUS-ETMX_L3_WIT_Y_DQ")
	channel_list.append("SUS-ETMX_L3_FASTIMON_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_FASTIMON_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_FASTIMON_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_FASTIMON_UR_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_NOISEMON_LL_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_NOISEMON_LR_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_NOISEMON_UL_OUT_DQ")
	channel_list.append("SUS-ETMX_L3_NOISEMON_UR_OUT_DQ")
	channel_list.append("CAL-PCALY_RX_PD_OUT_DQ")
	channel_list.append("CAL-PCALY_TX_PD_OUT_DQ")
	channel_list.append("CAL-PCALY_SWEPT_SINE_OFFSET")
	channel_list.append("CAL-PCALY_SWEPT_SINE_GAIN")
	channel_list.append("CAL-PCALY_SWEPT_SINE_LIMIT")
	channel_list.append("CAL-PCALY_SWEPT_SINE_TRAMP")
	channel_list.append("CAL-PCALY_SWEPT_SINE_SWREQ")
	channel_list.append("CAL-PCALY_SWEPT_SINE_SWMASK")
	channel_list.append("CAL-PCALY_SWEPT_SINE_INMON")
	channel_list.append("CAL-PCALY_SWEPT_SINE_EXCMON")
	channel_list.append("CAL-PCALY_SWEPT_SINE_OUT16")
	channel_list.append("CAL-PCALY_SWEPT_SINE_OUTPUT")
	channel_list.append("CAL-PCALY_SWEPT_SINE_SWSTAT")
	channel_list.append("CAL-PCALX_RX_PD_OUT_DQ")
	channel_list.append("CAL-PCALX_TX_PD_OUT_DQ")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE1_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALX_LINE1_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALX_LINE2_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE3_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE3_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALX_LINE3_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_COMPARISON_OSC_FREQ")
	channel_list.append("CAL-CS_TDEP_PCALX_LINE4_COMPARISON_OSC_FREQ")
	channel_list.append("SUS-ETMX_L1_CAL_LINE_FREQ")
	channel_list.append("SUS-ETMX_L2_CAL_LINE_FREQ")
	channel_list.append("SUS-ETMX_L3_CAL_LINE_FREQ")
	channel_list.append("SUS-ETMY_L1_CAL_LINE_FREQ")
	channel_list.append("SUS-ETMY_L2_CAL_LINE_FREQ")
	channel_list.append("SUS-ETMY_L3_CAL_LINE_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC1_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC2_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC3_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC4_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC5_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC6_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC7_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC8_OSC_FREQ")
	channel_list.append("CAL-PCALX_PCALOSC9_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC1_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC2_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC3_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC4_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC5_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC6_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC7_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC8_OSC_FREQ")
	channel_list.append("CAL-PCALY_PCALOSC9_OSC_FREQ")
	channel_list.append("PEM-EY_MAINSMON_EBAY_1_DQ")
	channel_list.append("PEM-EY_MAINSMON_EBAY_2_DQ")
	channel_list.append("PEM-EY_MAINSMON_EBAY_3_DQ")
	channel_list.append("PEM-EY_MAINSMON_EBAY_QUAD_SUM_DQ")
	channel_list.append("PEM-EX_MAINSMON_EBAY_1_DQ")
	channel_list.append("PEM-EX_MAINSMON_EBAY_2_DQ")
	channel_list.append("PEM-EX_MAINSMON_EBAY_3_DQ")
	channel_list.append("PEM-EX_MAINSMON_EBAY_QUAD_SUM_DQ")
	channel_list.append("PEM-CS_MAINSMON_EBAY_1_DQ")
	channel_list.append("PEM-CS_MAINSMON_EBAY_2_DQ")
	channel_list.append("PEM-CS_MAINSMON_EBAY_3_DQ")
	channel_list.append("PEM-CS_MAINSMON_EBAY_QUAD_SUM_DQ")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_CORRECTION_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_CORRECTION_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_CORRECTION_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_CORRECTION_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE3_CORRECTION_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE3_CORRECTION_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_CORRECTION_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_CORRECTION_IMAG")
	channel_list.append("SUS-ETMX_L1_CAL_LINE_SINGAIN")
	channel_list.append("SUS-ETMX_L2_CAL_LINE_SINGAIN")
	channel_list.append("SUS-ETMX_L3_CAL_LINE_SINGAIN")
	channel_list.append("SUS-ETMY_L1_CAL_LINE_SINGAIN")
	channel_list.append("SUS-ETMY_L2_CAL_LINE_SINGAIN")
	channel_list.append("SUS-ETMY_L3_CAL_LINE_SINGAIN")
	channel_list.append("SUS-ETMX_L1_CAL_LINE_CLKGAIN")
	channel_list.append("SUS-ETMX_L2_CAL_LINE_CLKGAIN")
	channel_list.append("SUS-ETMX_L3_CAL_LINE_CLKGAIN")
	channel_list.append("SUS-ETMY_L1_CAL_LINE_CLKGAIN")
	channel_list.append("SUS-ETMY_L2_CAL_LINE_CLKGAIN")
	channel_list.append("SUS-ETMY_L3_CAL_LINE_CLKGAIN")
	channel_list.append("CAL-PCALX_PCALOSC1_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC2_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC3_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC4_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC5_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC6_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC7_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC8_OSC_SINGAIN")
	channel_list.append("CAL-PCALX_PCALOSC9_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC1_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC2_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC3_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC4_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC5_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC6_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC7_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC8_OSC_SINGAIN")
	channel_list.append("CAL-PCALY_PCALOSC9_OSC_SINGAIN")
	channel_list.append("CAL-CS_TDEP_REF_INVA_CLGRATIO_TST_REAL")
	channel_list.append("CAL-CS_TDEP_REF_INVA_CLGRATIO_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_REF_CLGRATIO_CTRL_REAL")
	channel_list.append("CAL-CS_TDEP_REF_CLGRATIO_CTRL_IMAG")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_INV_REAL")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_INV_IMAG")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_REF_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_REF_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_REAL")
	channel_list.append("CAL-CS_TDEP_DARM_LINE1_REF_A_USUM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_C_NOCAVPOLE_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_C_NOCAVPOLE_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_D_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_D_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_A_USUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE2_REF_A_USUM_IMAG")
	channel_list.append("CAL-CS_TDEP_ESD_LINE1_REF_A_TST_NOLOCK_REAL")
	channel_list.append("CAL-CS_TDEP_ESD_LINE1_REF_A_TST_NOLOCK_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_C_NOCAVPOLE_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_C_NOCAVPOLE_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_D_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_D_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_A_USUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCALY_LINE4_REF_A_USUM_IMAG")
	channel_list.append("CAL-DARM_CTRL_WHITEN_OUT_DBL_DQ")
	channel_list.append("CAL-DARM_ERR_WHITEN_OUT_DBL_DQ")
	channel_list.append("CAL-DELTAL_EXTERNAL_DQ")
	channel_list.append("CAL-CFTD_DELTAL_EXTERNAL_DQ")
	channel_list.append("CAL-DELTAL_CTRL_TST_DBL_DQ")
	channel_list.append("CAL-DELTAL_CTRL_PUM_DBL_DQ")
	channel_list.append("CAL-DELTAL_CTRL_UIM_DBL_DQ")
	channel_list.append("CAL-DELTAL_RESIDUAL_DBL_DQ")
	channel_list.append("PSL-DIAG_BULLSEYE_WID_OUT_DQ")
	channel_list.append("PSL-DIAG_BULLSEYE_PIT_OUT_DQ")
	channel_list.append("PSL-DIAG_BULLSEYE_YAW_OUT_DQ")
	channel_list.append("PSL-ISS_SECONDLOOP_OUTPUT_DQ")
	channel_list.append("IMC-WFS_A_DC_PIT_OUT_DQ")
	channel_list.append("IMC-WFS_B_DC_PIT_OUT_DQ")
	channel_list.append("IMC-WFS_A_DC_YAW_OUT_DQ")
	channel_list.append("IMC-WFS_B_DC_YAW_OUT_DQ")
	channel_list.append("LSC-SRCL_IN1_DQ")
	channel_list.append("LSC-MICH_IN1_DQ")
	channel_list.append("LSC-PRCL_IN1_DQ")
	channel_list.append("LSC-DARM_IN1_DQ")
	channel_list.append("LSC-DARM_OUT_DQ")
	channel_list.append("ASC-DHARD_P_OUT_DQ")
	channel_list.append("ASC-DHARD_Y_OUT_DQ")
	channel_list.append("ASC-CHARD_P_OUT_DQ")
	channel_list.append("ASC-CHARD_Y_OUT_DQ")
	channel_list.append("ASC-CSOFT_P_OUT_DQ")
	channel_list.append("ASC-DSOFT_P_OUT_DQ")
	channel_list.append("ASC-INP1_P_INMON")
	channel_list.append("ASC-INP1_Y_INMON")
	channel_list.append("ASC-MICH_P_INMON")
	channel_list.append("ASC-MICH_Y_INMON")
	channel_list.append("ASC-PRC1_P_INMON")
	channel_list.append("ASC-PRC1_Y_INMON")
	channel_list.append("ASC-PRC2_P_INMON")
	channel_list.append("ASC-PRC2_Y_INMON")
	channel_list.append("ASC-SRC1_P_INMON")
	channel_list.append("ASC-SRC1_Y_INMON")
	channel_list.append("ASC-SRC2_P_INMON")
	channel_list.append("ASC-SRC2_Y_INMON")
	channel_list.append("ASC-DHARD_P_INMON")
	channel_list.append("ASC-DHARD_Y_INMON")
	channel_list.append("ASC-CHARD_P_INMON")
	channel_list.append("ASC-CHARD_Y_INMON")
	channel_list.append("ASC-DSOFT_P_INMON")
	channel_list.append("ASC-DSOFT_Y_INMON")
	channel_list.append("ASC-CSOFT_P_INMON")
	channel_list.append("ASC-CSOFT_Y_INMON")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NOCAVPOLE_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NOCAVPOLE_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_D_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_D_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NOLOCK_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NOLOCK_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_INVA_UIM_RESPRATIO_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_INVA_UIM_RESPRATIO_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NOLOCK_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NOLOCK_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_INVA_PUM_RESPRATIO_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_INVA_PUM_RESPRATIO_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NOLOCK_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NOLOCK_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_INVA_TST_RESPRATIO_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_INVA_TST_RESPRATIO_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_C_NOCAVPOLE_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_C_NOCAVPOLE_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_D_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_REF_D_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE4_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NOCAVPOLE_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NOCAVPOLE_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_D_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_D_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_A_TST_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_A_TST_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_A_PUM_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_A_PUM_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_A_UIM_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_A_UIM_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_TST_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_TST_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_PUM_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_PUM_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_UIM_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_UIM_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_RESP_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE1_REF_RESP_IMAG")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_RESP_REAL")
	channel_list.append("CAL-CS_TDEP_PCAL_LINE2_REF_RESP_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_RESP_OVER_A_TST_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_RESP_OVER_A_TST_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_RESP_OVER_A_PUM_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_RESP_OVER_A_PUM_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_RESP_OVER_A_UIM_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_RESP_OVER_A_UIM_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NL_IMAG")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NL_REAL")
	channel_list.append("CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NL_IMAG")
	channel_list.append("CAL-DARM_CTRL_DBL_DQ")
	channel_list.append("CAL-DARM_ERR_DBL_DQ")
	channel_list.append("CAL-CS_TDEP_SUS_LINE2_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_SUS_LINE3_UNCERTAINTY")
	channel_list.append("CAL-CS_TDEP_KAPPA_UIM_REAL_OUTPUT")
	channel_list.append("CAL-CS_TDEP_KAPPA_UIM_IMAG_OUTPUT")
	channel_list.append("CAL-CS_TDEP_KAPPA_PUM_REAL_OUTPUT")
	channel_list.append("CAL-CS_TDEP_KAPPA_PUM_IMAG_OUTPUT")
	channel_list.append("CAL-CS_TDEP_KAPPA_TST_REAL_OUTPUT")
	channel_list.append("CAL-CS_TDEP_KAPPA_TST_IMAG_OUTPUT")
	channel_list.append("CAL-CS_TDEP_KAPPA_C_OUTPUT")
	channel_list.append("CAL-CS_TDEP_F_C_OUTPUT")
	channel_list.append("CAL-CS_TDEP_F_S_OUTPUT")
	channel_list.append("CAL-CS_TDEP_Q_S_OUTPUT")
	channel_list.append("GRD-IFO_OK")
	channel_list.append("GRD-ISC_LOCK_STATE_N")
	channel_list.append("GRD-ISC_LOCK_NOMINAL_N")
	channel_list.append("GRD-ISC_LOCK_STATUS")
	channel_list.append("GRD-ISC_LOCK_OK")
	channel_list.append("GRD-ISC_LOCK_ERROR")
	channel_list.append("GRD-IFO_INTENT")
	channel_list.append("GRD-IFO_READY")
	channel_list.append("GRD-IFO_UNDISTURBED_OK")
	temp_list = channel_list
	channel_list = []
	for chan in temp_list:
		if chan in available_channels:
			channel_list.append(chan)

	print("Finished channel list")

# In case we are writing EPICS channels from a filters file.
filters_name = options.filters_file
if filters_name is not None:
	if filters_name == "None":
		filters_name = None
if filters_name is not None:
	# Search the directory tree for files with names matching the one we want.
	if filters_name.count('/') > 0:
		# Then the path to the filters file was given
		filters = numpy.load(filters_name)
	else:
		# We need to search for the filters file
		filters_paths = []
		print("\nSearching for %s ..." % filters_name)
		# Check the user's home directory
		for dirpath, dirs, files in os.walk(os.environ['HOME']):
			if filters_name in files:
				# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
				if dirpath.count("GDSFilters") > 0:
					filters_paths.insert(0, os.path.join(dirpath, filters_name))
				else:
					filters_paths.append(os.path.join(dirpath, filters_name))
		# Check if there is a checkout of the entire calibration SVN
		for dirpath, dirs, files in os.walk('/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/'):
			if filters_name in files:
				# We prefer filters that came directly from a GDSFilters directory of the calibration SVN
				if dirpath.count("GDSFilters") > 0:
					filters_paths.insert(0, os.path.join(dirpath, filters_name))
				else:
					filters_paths.append(os.path.join(dirpath, filters_name))
		if not len(filters_paths):
			raise ValueError("Cannot find filters file %s in home directory %s or in /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/*/GDSFilters", (filters_name, os.environ['HOME']))
		print("Loading calibration filters from %s\n" % filters_paths[0])
		filters = numpy.load(filters_paths[0])

EPICS_list = []
EPICS_channel_list = []
def add_EPICS(filters, EPICS_name, EPICS_channel_name):
	if "%s_real" % EPICS_name in filters:
		EPICS_list.append(float(filters["%s_real" % EPICS_name]))
		EPICS_list.append(float(filters["%s_imag" % EPICS_name]))
		EPICS_channel_list.append("%s_REAL" % EPICS_channel_name)
		EPICS_channel_list.append("%s_IMAG" % EPICS_channel_name)

if filters_name is not None:
	add_EPICS(filters, 'EP1', 'CAL-CS_TDEP_SUS_LINE3_REF_INVA_TST_RESPRATIO')
	add_EPICS(filters, 'EP6', 'CAL-CS_TDEP_PCAL_LINE2_REF_C_NOCAVPOLE')
	add_EPICS(filters, 'EP7', 'CAL-CS_TDEP_PCAL_LINE2_REF_D')
	add_EPICS(filters, 'EP8', 'CAL-CS_TDEP_PCAL_LINE2_REF_A_TST')
	add_EPICS(filters, 'EP10', 'CAL-CS_TDEP_SUS_LINE3_REF_A_TST_NL')
	add_EPICS(filters, 'EP11', 'CAL-CS_TDEP_PCAL_LINE1_REF_C_NOCAVPOLE')
	add_EPICS(filters, 'EP12', 'CAL-CS_TDEP_PCAL_LINE1_REF_D')
	add_EPICS(filters, 'EP13', 'CAL-CS_TDEP_PCAL_LINE1_REF_A_TST')
	add_EPICS(filters, 'EP15', 'CAL-CS_TDEP_SUS_LINE2_REF_INVA_PUM_RESPRATIO')
	add_EPICS(filters, 'EP18', 'CAL-CS_TDEP_PCAL_LINE2_REF_A_PUM')
	add_EPICS(filters, 'EP19', 'CAL-CS_TDEP_PCAL_LINE2_REF_A_UIM')
	add_EPICS(filters, 'EP20', 'CAL-CS_TDEP_PCAL_LINE1_REF_A_PUM')
	add_EPICS(filters, 'EP21', 'CAL-CS_TDEP_PCAL_LINE1_REF_A_UIM')
	add_EPICS(filters, 'EP22', 'CAL-CS_TDEP_SUS_LINE1_REF_INVA_UIM_RESPRATIO')
	add_EPICS(filters, 'EP23', 'CAL-CS_TDEP_SUS_LINE2_REF_A_PUM_NL')
	add_EPICS(filters, 'EP24', 'CAL-CS_TDEP_SUS_LINE1_REF_A_UIM_NL')
	add_EPICS(filters, 'EP25', 'CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_TST')
	add_EPICS(filters, 'EP26', 'CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_PUM')
	add_EPICS(filters, 'EP27', 'CAL-CS_TDEP_PCAL_LINE1_REF_C_NCP_D_A_UIM')
	add_EPICS(filters, 'EP28', 'CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_TST')
	add_EPICS(filters, 'EP29', 'CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_PUM')
	add_EPICS(filters, 'EP30', 'CAL-CS_TDEP_PCAL_LINE2_REF_C_NCP_D_A_UIM')
	add_EPICS(filters, 'EP31', 'CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_A_TST_NL')
	add_EPICS(filters, 'EP32', 'CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_TST')
	add_EPICS(filters, 'EP33', 'CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_PUM')
	add_EPICS(filters, 'EP34', 'CAL-CS_TDEP_SUS_LINE3_REF_C_NCP_D_A_UIM')
	add_EPICS(filters, 'EP35', 'CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_A_PUM_NL')
	add_EPICS(filters, 'EP36', 'CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_TST')
	add_EPICS(filters, 'EP37', 'CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_PUM')
	add_EPICS(filters, 'EP38', 'CAL-CS_TDEP_SUS_LINE2_REF_C_NCP_D_A_UIM')
	add_EPICS(filters, 'EP39', 'CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_A_UIM_NL')
	add_EPICS(filters, 'EP40', 'CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_TST')
	add_EPICS(filters, 'EP41', 'CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_PUM')
	add_EPICS(filters, 'EP42', 'CAL-CS_TDEP_SUS_LINE1_REF_C_NCP_D_A_UIM')
	add_EPICS(filters, 'EP43', 'CAL-CS_TDEP_PCAL_LINE1_REF_RESP')
	add_EPICS(filters, 'EP44', 'CAL-CS_TDEP_PCAL_LINE2_REF_RESP')
	add_EPICS(filters, 'EP45', 'CAL-CS_TDEP_SUS_LINE3_REF_RESP_OVER_A_TST_NL')
	add_EPICS(filters, 'EP46', 'CAL-CS_TDEP_SUS_LINE2_REF_RESP_OVER_A_PUM_NL')
	add_EPICS(filters, 'EP47', 'CAL-CS_TDEP_SUS_LINE1_REF_RESP_OVER_A_UIM_NL')


ifo_channel_list = []
channel_list_copy = channel_list.copy()
for chan in channel_list_copy:
	if chan in EPICS_channel_list:
		channel_list.remove(chan)
	else:
		ifo_channel_list.append((ifo, chan))


def frame_manipulator(pipeline, name):

	#
	# This pipeline reads the channels needed for calibration from the raw frames
	# and writes them to smaller frames for faster access.
	#

	head_dict = {}

	# Get the data from the raw frames and pick out the channels we want
	src = pipeparts.mklalcachesrc(pipeline, location = frame_cache, cache_dsc_regex = ifo)
	src = pipeparts.mkprogressreport(pipeline, src, "start")
	demux = pipeparts.mkframecppchanneldemux(pipeline, src, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, ifo_channel_list)))

	# Make a muxer to collect the channels we need
	channelmux_input_dict = {}
	have_ones = False
	for key, chan in zip(channel_list, channel_list):
		head_dict[key] = calibration_parts.hook_up(pipeline, demux, chan, ifo, 1.0)
		if filters_name is not None and not have_ones and "CAL-CS_TDEP_" in chan:
			# Make a stream of ones from which to make new channels
			ones = head_dict[key] = pipeparts.mktee(pipeline, head_dict[key])
			ones = calibration_parts.mkinsertgap(pipeline, ones, insert_gap = False, replace_value = 1.0, bad_data_intervals = [1.0, 1.0])
			ones = pipeparts.mktee(pipeline, ones)
			have_ones = True
		head_dict[key] = pipeparts.mkprogressreport(pipeline, head_dict[key], "before muxer %s" % key)
		channelmux_input_dict["%s:%s" % (ifo, chan)] = calibration_parts.mkqueue(pipeline, calibration_parts.mkinsertgap(pipeline, head_dict[key], insert_gap = False, chop_length = 64 * 1e9))

	for i in range(len(EPICS_channel_list)):
		head_dict[EPICS_channel_list[i]] = pipeparts.mkaudioamplify(pipeline, ones, EPICS_list[i])
		head_dict[EPICS_channel_list[i]] = pipeparts.mkprogressreport(pipeline, head_dict[EPICS_channel_list[i]], "before muxer %s" % EPICS_channel_list[i])
		channelmux_input_dict["%s:%s" % (ifo, EPICS_channel_list[i])] = calibration_parts.mkqueue(pipeline, calibration_parts.mkinsertgap(pipeline, head_dict[EPICS_channel_list[i]], insert_gap = False, chop_length = 64 * 1e9))

	if '.txt' in output_path:
		for chan in channel_list + EPICS_channel_list:
			pipeparts.mknxydumpsink(pipeline, channelmux_input_dict["%s:%s" % (ifo, chan)], "%s-%s%s" % (ifo, chan, output_path))
	else:
		mux = pipeparts.mkframecppchannelmux(pipeline, channelmux_input_dict, frame_duration = frame_length, frames_per_file = frames_per_file, compression_scheme = 6, compression_level = 3)
		mux = pipeparts.mkprogressreport(pipeline, mux, "end")
		pipeparts.mkframecppfilesink(pipeline, mux, frame_type = frame_type, path = output_path, instrument = ifo)

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


test_common.build_and_run(frame_manipulator, "frame_manipulator", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))


