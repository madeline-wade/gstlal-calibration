/*
 * Copyright (C) 2023 Aaron Viets <aaron.viets@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/*
 * ============================================================================
 *
 *				  Preamble
 *
 * ============================================================================
 */


/*
 * stuff from C
 */


#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>


/*
 *  stuff from gobject/gstreamer
 */


#include <glib.h>
#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <gst/base/gstbasetransform.h>


/*
 * our own stuff
 */


#include <lal/Date.h>
#include <gstlal_pacer.h>


/*
 * ============================================================================
 *
 *			   GStreamer Boiler Plate
 *
 * ============================================================================
 */


#define GST_CAT_DEFAULT gstlal_pacer_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);


G_DEFINE_TYPE_WITH_CODE(
	GSTLALPacer,
	gstlal_pacer,
	GST_TYPE_BASE_TRANSFORM,
	GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "lal_pacer", 0, "lal_pacer element")
);


static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE(
	GST_BASE_TRANSFORM_SINK_NAME,
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS("ANY")
);


static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE(
	GST_BASE_TRANSFORM_SRC_NAME,
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS("ANY")
);


/*
 * ============================================================================
 *
 *		     GstBaseTransform Method Overrides
 *
 * ============================================================================
 */


/*
 * transform_ip()
 */


static GstFlowReturn transform_ip(GstBaseTransform *trans, GstBuffer *buf) {

	GSTLALPacer *element = GSTLAL_PACER(trans);
	GstFlowReturn result = GST_FLOW_OK;

	GstDateTime *current_gst_time;
	gchar *current_utc_time;
	struct tm tm;
	guint64 current_time, stream_time, time_to_push_buffer, wait_time;

	/* Get the current real time as a string */
	current_gst_time = gst_date_time_new_now_utc();
	current_utc_time = gst_date_time_to_iso8601_string(current_gst_time);

	/* parse DateTime to gps time */
	strptime(current_utc_time, "%Y-%m-%dT%H:%M:%SZ", &tm);

	/* Time in nanoseconds */
	current_time = (guint64) XLALUTCToGPS(&tm) * 1000000000 + (guint64) gst_date_time_get_microsecond(current_gst_time) * 1000;
	gst_date_time_unref(current_gst_time);
	g_free(current_utc_time);

	/* Stream time of the end of the current buffer */
	stream_time = GST_BUFFER_PTS(buf) + GST_BUFFER_DURATION(buf);

	if(G_UNLIKELY(GST_BUFFER_IS_DISCONT(buf) || !GST_CLOCK_TIME_IS_VALID(element->t0))) {
		/* Stream time of the end of the first buffer */
		element->t0 = stream_time;
		element->real_t0 = current_time;
	}

	time_to_push_buffer = element->real_t0 + (stream_time - element->t0) / element->speed;
	wait_time = time_to_push_buffer > current_time ? time_to_push_buffer - current_time : 0;

	sleep(wait_time / 1000000000.0);

	return result;
}


/*
 * ============================================================================
 *
 *			  GObject Method Overrides
 *
 * ============================================================================
 */


enum property {
	ARG_SPEED = 1,
};


/*
 * set_property()
 */


static void set_property(GObject *object, enum property prop_id, const GValue *value, GParamSpec *pspec) {

	GSTLALPacer *element = GSTLAL_PACER(object);

	GST_OBJECT_LOCK(element);

	switch (prop_id) {
	case ARG_SPEED:
		element->speed = g_value_get_double(value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


/*
 * get_property()
 */


static void get_property(GObject *object, enum property prop_id, GValue *value, GParamSpec *pspec) {

	GSTLALPacer *element = GSTLAL_PACER(object);

	GST_OBJECT_LOCK(element);

	switch (prop_id) {
	case ARG_SPEED:
		g_value_set_double(value, element->speed);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


/*
 * class_init()
 */


static void gstlal_pacer_class_init(GSTLALPacerClass *klass) {

	GstBaseTransformClass *transform_class = GST_BASE_TRANSFORM_CLASS(klass);
	GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

	gst_element_class_set_details_simple(
		element_class,
		"Pacer",
		"Filter/Audio",
		"Controls the real-time rate at which data is processed",
		"Aaron Viets <aaron.viets@ligo.org>"
	);

	gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
	gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);

	g_object_class_install_property(
		gobject_class,
		ARG_SPEED,
		g_param_spec_double(
			"speed",
			"Speed",
			"Rate relative to real-time at which to process data.  Setting this to\n\t\t\t"
			"1.0 will cause data to be processed at real-time rate.",
			0.0, G_MAXDOUBLE, 1.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
		)
	);

	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_factory));
	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&sink_factory));

	transform_class->transform_ip = GST_DEBUG_FUNCPTR(transform_ip);
}


/*
 * init()
 */


static void gstlal_pacer_init(GSTLALPacer *element) {
	element->t0 = GST_CLOCK_TIME_NONE;
	gst_base_transform_set_gap_aware(GST_BASE_TRANSFORM(element), TRUE);
}


