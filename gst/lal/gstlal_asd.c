/*
 * Copyright (C) 2021  Aaron Viets <aaron.viets@ligo.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/*
 * ========================================================================
 *
 *				  Preamble
 *
 * ========================================================================
 */


/*
 * stuff from the C library
 */


#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>


/*
 * stuff from glib/gstreamer
 */


#include <glib.h>
#include <glib/gprintf.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/audio/audio.h>
#include <gst/audio/audio-format.h>


/*
 * our own stuff
 */


#include <gstlal/gstlal.h>
#include <gstlal/gstlal_audio_info.h>
#include <gstlal/gstlal_debug.h>
#include <gstlal_firtools.h>
#include <gstlal_asd.h>


/*
 * ============================================================================
 *
 *			    Custom Types
 *
 * ============================================================================
 */


/*
 * window type enum
 */


GType gstlal_asd_window_get_type(void) {

	static GType type = 0;

	if(!type) {
		static GEnumValue values[] = {
			{GSTLAL_ASD_DPSS, "GSTLAL_ASD_DPSS", "Maximize energy concentration in main lobe"},
			{GSTLAL_ASD_KAISER, "GSTLAL_ASD_KAISER", "Simple approximtion to DPSS window"},
			{GSTLAL_ASD_DOLPH_CHEBYSHEV, "GSTLAL_ASD_DOLPH_CHEBYSHEV", "Attenuate all side lobes equally"},
			{GSTLAL_ASD_BLACKMAN, "GSTLAL_ASD_BLACKMAN", "Strongly attenuate distant side lobes"},
			{GSTLAL_ASD_HANN, "GSTLAL_ASD_HANN", "Cosine squared window"},
			{GSTLAL_ASD_NONE, "GSTLAL_ASD_NONE", "Do not apply a window function"},
			{0, NULL, NULL}
		};

		type = g_enum_register_static("GSTLAL_ASD_WINDOW", values);
	}

	return type;
}


/*
 * ============================================================================
 *
 *			   GStreamer Boilerplate
 *
 * ============================================================================
 */


#define GST_CAT_DEFAULT gstlal_asd_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);


G_DEFINE_TYPE_WITH_CODE(
	GSTLALASD,
	gstlal_asd,
	GST_TYPE_BASE_SINK,
	GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "lal_asd", 0, "lal_asd element")
);


enum property {
	ARG_FFT_SAMPLES = 1,
	ARG_OVERLAP_SAMPLES,
	ARG_UPDATE_TIME,
	ARG_WINDOW_TYPE,
	ARG_FREQUENCY_RESOLUTION,
	ARG_ASD,
	ARG_ASD_ENDTIME,
	ARG_FILENAME,
	ARG_WHITEN_FILTER,
	ARG_MEDIAN_SAMPLES,
	ARG_FAKE
};


static GParamSpec *properties[ARG_FAKE];


/*
 * ============================================================================
 *
 *				  Utilities
 *
 * ============================================================================
 */


static void write_asd(double *asd, gint rate, guint64 rows, char *filename) {

	guint64 i;
	FILE *fp;
	fp = fopen(filename, "a");

	for(i = 0; i < rows; i++)
		g_fprintf(fp, "%1.7e\t%1.15e\n", (double) rate / 2 * i / (rows - 1), (double) asd[i]);

	g_fprintf(fp, "\n\n\n");

	fclose(fp);

	return;
}


double asd_median(double *asd, guint64 samples) {

	guint64 i, j;
	double temp;
	double *copy = g_malloc(samples * sizeof(double));
	for(i = 0; i < samples; i++)
		copy[i] = asd[i];

	/* Put the array in order */
	for(i = 0; i < samples - 1; i++) {
		for(j = 0; j < samples - 1; j++) {
			if(copy[j] > copy[j + 1]) {
				temp = copy[j + 1];
				copy[j + 1] = copy[j];
				copy[j] = temp;
			}
		}
	}

	/* Pick the central value(s) */
	if(samples % 2)
		return copy[samples / 2];
	else
		return (copy[samples / 2 - 1] + copy[samples / 2]) / 2.0;
}


static void update_whitening_filter(GSTLALASD *element) {

	int fd_samples = (int) element->filter_samples / 2 + 1;
	complex double *whiten_filter_fd = g_malloc0(fd_samples * sizeof(complex double));
	int i, start_index;
	for(i = 1; i < fd_samples - 1; i++) {
		start_index = (int) (element->fft_samples / 2 * ((double) i / (fd_samples - 1)) + 0.5) - (int) element->median_samples / 2;
		/* Inverse of ASD used to whiten.  Negating every other sample centers the filter in time. */
		whiten_filter_fd[i] = (i % 2 ? -1.0 : 1.0) / asd_median(element->asd + start_index, element->median_samples);
	}

	element->whiten_filter = gstlal_irfft_double(whiten_filter_fd, (guint) fd_samples, NULL, NULL, 0, NULL, TRUE, 0, NULL, 0, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, FALSE);
	kaiser_double(element->filter_samples, 10, element->whiten_filter, FALSE);

	return;
}


#define DEFINE_PROCESS_INPUT_DATA(DTYPE) \
static void process_input_data_ ## DTYPE(GSTLALASD *element, DTYPE *src, guint64 src_size) { \
 \
	DTYPE *little_asd; \
	gint64 i, num_asds; \
	guint64 j, src_index = 0; \
	guint64 asd_samples = element->fft_samples / 2 + 1; \
 \
	/* How many "little ASDs" will we compute with this buffer? */ \
	num_asds = (gint64) (element->input_index + src_size - element->overlap_samples) / (gint64) (element->fft_samples - element->overlap_samples); \
	num_asds = num_asds > 0 ? num_asds : 0; \
 \
	/* Add in the new little ASDs */ \
	for(i = 0; i < num_asds; i++) { \
		/* Fill in the fft input with data */ \
		while(element->input_index < element->fft_samples) { \
			element->workspace.DTYPE ## p.fft_input[element->input_index] = src[src_index]; \
			element->input_index++; \
			src_index++; \
		} \
 \
		little_asd = gstlal_asd_ ## DTYPE(element->workspace.DTYPE ## p.fft_input, element->fft_samples, element->rate, element->fft_samples, element->overlap_samples, element->workspace.DTYPE ## p.window); \
 \
		/* Add this into the ASD */ \
		for(j = 0; j < asd_samples; j++) \
			element->asd[j] += (double) little_asd[j]; \
		g_free(little_asd); \
		element->input_index = 0; \
		if(src_index >= element->overlap_samples) \
			src_index -= element->overlap_samples; \
		else { \
			for(element->input_index = 0; element->input_index < element->overlap_samples - src_index; element->input_index++) \
				element->workspace.DTYPE ## p.fft_input[element->input_index] = element->workspace.DTYPE ## p.fft_input[element->input_index + element->fft_samples - element->overlap_samples]; \
			src_index = 0; \
		} \
	} \
	element->num_ffts_in_avg += num_asds; \
 \
	/* Whatever data remains in src is put in storage for later */ \
	while(src_index < src_size) { \
		element->workspace.DTYPE ## p.fft_input[element->input_index] = src[src_index]; \
		element->input_index++; \
		src_index++; \
	} \
	g_assert_cmpuint(element->input_index, <, element->fft_samples); \
 \
	return; \
}


DEFINE_PROCESS_INPUT_DATA(float);
DEFINE_PROCESS_INPUT_DATA(double);


/*
 * ============================================================================
 *
 *			    GstBaseSink Overrides
 *
 * ============================================================================
 */


/*
 * get_unit_size()
 */


static gboolean get_unit_size(GstBaseSink *sink, GstCaps *caps, gsize *size) {

	GstAudioInfo info;
	gboolean success = gstlal_audio_info_from_caps(&info, caps);
	if(success)
		*size = GST_AUDIO_INFO_BPF(&info);
	else
		GST_WARNING_OBJECT(sink, "unable to parse caps %" GST_PTR_FORMAT, caps);
	return success;
}


/*
 * event()
 */


static gboolean event(GstBaseSink *sink, GstEvent *event) {

	GSTLALASD *element = GSTLAL_ASD(sink);
	GST_DEBUG_OBJECT(element, "Got %s event on sink pad", GST_EVENT_TYPE_NAME(event));

	guint64 i, asd_samples = element->fft_samples / 2 + 1;

	if(GST_EVENT_TYPE(event) == GST_EVENT_EOS && element->num_ffts_in_avg > 0) {
		/* Divide by the number of ASDs added */
		for(i = 0; i < asd_samples; i++)
			element->asd[i] /= element->num_ffts_in_avg;

		/*  Update the whitening filter */
		update_whitening_filter(element);
		/* Let other elements know about the update */
		g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_ASD]);
		g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_WHITEN_FILTER]);
		/* The last ASD never becomes invalid */
		element->asd_endtime = G_MAXUINT64;
		g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_ASD_ENDTIME]);

		/* Write to file */
		write_asd(element->asd, element->rate, asd_samples, element->filename);
	}

	return GST_BASE_SINK_CLASS(gstlal_asd_parent_class)->event(sink, event);
}


/*
 * set_caps()
 */


static gboolean set_caps(GstBaseSink *sink, GstCaps *caps) {

	GSTLALASD *element = GSTLAL_ASD(sink);
	double alpha;
	guint64 i;

	gboolean success = TRUE;

	gsize unit_size;

	/* Parse the caps to find the format, sample rate, and number of channels */
	GstStructure *str = gst_caps_get_structure(caps, 0);
	const gchar *name = gst_structure_get_string(str, "format");
	success &= (name != NULL);
	success &= gst_structure_get_int(str, "rate", &element->rate);

	/* Find unit size */
	success &= get_unit_size(sink, caps, &unit_size);
	element->unit_size = unit_size;

	/* Record the data type */
	if(success) {
		if(!strcmp(name, GST_AUDIO_NE(F32)))
			element->data_type = GSTLAL_ASD_F32;
		else if(!strcmp(name, GST_AUDIO_NE(F64)))
			element->data_type = GSTLAL_ASD_F64;
		else
			g_assert_not_reached();
	}

	/* Convert update_time to samples */
	if(element->update_time == G_MAXUINT64)
		element->update_samples = G_MAXUINT64;
	else
		element->update_samples = element->update_time * element->rate;

	/* If we are writing output to file, and a file already exists with the same name, remove it */
	if(element->filename)
		remove(element->filename);

	/* Sanity check */
	if(element->overlap_samples >= element->fft_samples) {
		GST_WARNING_OBJECT(element, "overlap_samples cannot be greater than fft_samples. Resetting overlap_samples to fft_samples / 2.");
		element->overlap_samples = element->fft_samples / 2;
	}
	if(element->fft_samples * element->frequency_resolution < element->rate) {
		if(element->frequency_resolution > 0.0) {
			/*
			 * The user set an invalid frequency resolution, so give a warning and
			 * set the finest resolution that is within reason.
			 */
			GST_WARNING_OBJECT(element, "frequency-resolution is too fine.  Resetting frequency-resolution to be equal to the inverse of the FFT length in seconds.");
			element->frequency_resolution = (double) element->rate / element->fft_samples;
		} else
			/* This is the default value, so set the resolution to a typical value. */
			element->frequency_resolution = 3.0 * element->rate / element->fft_samples;
	}

	/*
	 * Memory allocation
	 */

	element->asd = g_malloc0((element->fft_samples / 2 + 1) * sizeof(double));
	element->filter_samples = element->fft_samples / element->median_samples;
	/* Make the filter an even length */
	element->filter_samples += element->filter_samples % 2;
	if(element->data_type == GSTLAL_ASD_F32) {

		/* Space to store data from previous buffers needed for future FFTs */
		element->workspace.floatp.fft_input = g_malloc(element->fft_samples * sizeof(float));

		/* Make a window function as specified by element properties */
		/* Frequency resolution in units of frequency bins of fft data */
		alpha = element->frequency_resolution * element->fft_samples / element->rate;
		switch(element->window_type) {
		case GSTLAL_ASD_DPSS:
			element->workspace.floatp.window = dpss_float(element->fft_samples, alpha, 5.0, NULL, FALSE, FALSE);
			break;

		case GSTLAL_ASD_KAISER:
			element->workspace.floatp.window = kaiser_float(element->fft_samples, M_PI * alpha, NULL, FALSE);
			break;

		case GSTLAL_ASD_DOLPH_CHEBYSHEV:
			element->workspace.floatp.window = DolphChebyshev_float(element->fft_samples, alpha, NULL, FALSE);
			break;

		case GSTLAL_ASD_BLACKMAN:
			element->workspace.floatp.window = blackman_float(element->fft_samples, NULL, FALSE);
			break;

		case GSTLAL_ASD_HANN:
			element->workspace.floatp.window = hann_float(element->fft_samples, NULL, FALSE);
			break;

		case GSTLAL_ASD_NONE:
			element->workspace.floatp.window = g_malloc(element->fft_samples * sizeof(float));
			for(i = 0; i < element->fft_samples; i++)
				element->workspace.floatp.window[i] = 1.0;
			break;

		default:
			GST_ERROR_OBJECT(element, "Invalid window type.  See properties for appropriate window types.");
			g_assert_not_reached();
			break;
		}
	} else if(element->data_type == GSTLAL_ASD_F64) {

		/* Space to store data from previous buffers needed for future FFTs */
		element->workspace.doublep.fft_input = g_malloc(element->fft_samples * sizeof(double));

		/* Make a window function as specified by element properties */
		/* Frequency resolution in units of frequency bins of fft data */
		alpha = element->frequency_resolution * element->fft_samples / element->rate;
		switch(element->window_type) {
		case GSTLAL_ASD_DPSS:
			element->workspace.doublep.window = dpss_double(element->fft_samples, alpha, 5.0, NULL, FALSE, FALSE);
			break;

		case GSTLAL_ASD_KAISER:
			element->workspace.doublep.window = kaiser_double(element->fft_samples, M_PI * alpha, NULL, FALSE);
			break;

		case GSTLAL_ASD_DOLPH_CHEBYSHEV:
			element->workspace.doublep.window = DolphChebyshev_double(element->fft_samples, alpha, NULL, FALSE);
			break;

		case GSTLAL_ASD_BLACKMAN:
			element->workspace.doublep.window = blackman_double(element->fft_samples, NULL, FALSE);
			break;

		case GSTLAL_ASD_HANN:
			element->workspace.doublep.window = hann_double(element->fft_samples, NULL, FALSE);
			break;

		case GSTLAL_ASD_NONE:
			element->workspace.doublep.window = g_malloc(element->fft_samples * sizeof(double));
			for(i = 0; i < element->fft_samples; i++)
				element->workspace.doublep.window[i] = 1.0;
			break;

		default:
			GST_ERROR_OBJECT(element, "Invalid window type.  See properties for appropriate window types.");
			g_assert_not_reached();
			break;
		}
	} else
		success = FALSE;

	return success;
}


/*
 * render()
 */


static GstFlowReturn render(GstBaseSink *sink, GstBuffer *buffer) {

	GSTLALASD *element = GSTLAL_ASD(sink);
	GstMapInfo mapinfo;
	GstFlowReturn result = GST_FLOW_OK;

	/*
	 * check for discontinuity
	 */

	if(G_UNLIKELY(GST_BUFFER_IS_DISCONT(buffer) || GST_BUFFER_OFFSET(buffer) != element->next_in_offset || !GST_CLOCK_TIME_IS_VALID(element->t0))) {
		element->t0 = GST_BUFFER_PTS(buffer);
		element->offset0 = GST_BUFFER_OFFSET(buffer);
		element->input_index = 0;
	}
	element->next_in_offset = GST_BUFFER_OFFSET_END(buffer);
	GST_DEBUG_OBJECT(element, "have buffer spanning %" GST_BUFFER_BOUNDARIES_FORMAT, GST_BUFFER_BOUNDARIES_ARGS(buffer));

	/* Check for gaps */
	if(!GST_BUFFER_FLAG_IS_SET(buffer, GST_BUFFER_FLAG_GAP) && mapinfo.size) {
		/* Get the data from the buffer */
		gst_buffer_map(buffer, &mapinfo, GST_MAP_READ);

		if(element->data_type == GSTLAL_ASD_F32)
			process_input_data_float(element, (float *) mapinfo.data, mapinfo.size / element->unit_size);
		else if(element->data_type == GSTLAL_ASD_F64)
			process_input_data_double(element, (double *) mapinfo.data, mapinfo.size / element->unit_size);
		else
			g_assert_not_reached();

		gst_buffer_unmap(buffer, &mapinfo);

	} else if(mapinfo.size)
		/* Then there is a gap and we need to drop stored data */
		element->input_index = 0;

	/* Should ASD be updated at the end of this buffer? */
	if(element->update_samples - gst_util_uint64_scale_int_round(GST_BUFFER_PTS(buffer), element->rate, GST_SECOND) % element->update_samples <= mapinfo.size / element->unit_size && element->num_ffts_in_avg > 0) {

		guint64 i, asd_samples = element->fft_samples / 2 + 1;

		/* Divide by the number of ASDs added */
		for(i = 0; i < asd_samples; i++)
			element->asd[i] /= element->num_ffts_in_avg;

		/*  Update the whitening filter */
		update_whitening_filter(element);
		/* Let other elements know about the update */
		g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_ASD]);
		g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_WHITEN_FILTER]);
		/* Provide a timestamp indicating when the ASD becomes invalid */
		element->asd_endtime = GST_BUFFER_PTS(buffer) + GST_BUFFER_DURATION(buffer) + GST_SECOND * element->update_time;
		g_object_notify_by_pspec(G_OBJECT(element), properties[ARG_ASD_ENDTIME]);

		/* Write to file */
		write_asd(element->asd, element->rate, asd_samples, element->filename);

		/* Reset */
		element->num_ffts_in_avg = 0;
		for(i = 0; i < asd_samples; i++)
			element->asd[i] = 0.0;
	}

	return result;
}


/*
 * stop()
 */


static gboolean stop(GstBaseSink *sink) {

	GSTLALASD *element = GSTLAL_ASD(sink);

	if(element->asd) {
		g_free(element->asd);
		element->asd = NULL;
	}
	if(element->whiten_filter) {
		g_free(element->whiten_filter);
		element->whiten_filter = NULL;
	}

	if(element->data_type == GSTLAL_ASD_F64) {
		if(element->window_type == GSTLAL_ASD_DPSS)
			dpss_double(0, 0.0, 0.0, NULL, FALSE, TRUE);

		if(element->workspace.doublep.window) {
			g_free(element->workspace.doublep.window);
			element->workspace.doublep.window = NULL;
		}
		if(element->workspace.doublep.fft_input) {
			g_free(element->workspace.doublep.fft_input);
			element->workspace.doublep.fft_input = NULL;
		}
	} else if(element->data_type == GSTLAL_ASD_F32) {
		if(element->window_type == GSTLAL_ASD_DPSS)
			dpss_float(0, 0.0, 0.0, NULL, FALSE, TRUE);

		if(element->workspace.floatp.window) {
			g_free(element->workspace.floatp.window);
			element->workspace.floatp.window = NULL;
		}
		if(element->workspace.floatp.fft_input) {
			g_free(element->workspace.floatp.fft_input);
			element->workspace.floatp.fft_input = NULL;
		}
	}

	return TRUE;
}


/*
 * ============================================================================
 *
 *			      GObject Methods
 *
 * ============================================================================
 */


/*
 * properties
 */


static void set_property(GObject *object, enum property id, const GValue *value, GParamSpec *pspec) {

	GSTLALASD *element = GSTLAL_ASD(object);

	GST_OBJECT_LOCK(element);

	switch(id) {
	case ARG_FFT_SAMPLES:
		element->fft_samples = g_value_get_uint64(value);
		break;

	case ARG_OVERLAP_SAMPLES:
		element->overlap_samples = g_value_get_uint64(value);
		break;

	case ARG_UPDATE_TIME:
		element->update_time = g_value_get_uint64(value);
		break;

	case ARG_WINDOW_TYPE:
		element->window_type = g_value_get_enum(value);
		break;

	case ARG_FREQUENCY_RESOLUTION:
		element->frequency_resolution = g_value_get_double(value);
		break;

	case ARG_FILENAME:
		element->filename = g_value_dup_string(value);
		break;

	case ARG_MEDIAN_SAMPLES:
		element->median_samples = g_value_get_uint64(value);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


static void get_property(GObject *object, enum property id, GValue *value, GParamSpec *pspec) {

	GSTLALASD *element = GSTLAL_ASD(object);

	GST_OBJECT_LOCK(element);

	switch(id) {
	case ARG_FFT_SAMPLES:
		g_value_set_uint64(value, element->fft_samples);
		break;

	case ARG_OVERLAP_SAMPLES:
		g_value_set_uint64(value, element->overlap_samples);
		break;

	case ARG_UPDATE_TIME:
		g_value_set_uint64(value, element->update_time);
		break;

	case ARG_WINDOW_TYPE:
		g_value_set_enum(value, element->window_type);
		break;

	case ARG_FREQUENCY_RESOLUTION:
		g_value_set_double(value, element->frequency_resolution);
		break;

	case ARG_ASD: ;
		GValue asd = G_VALUE_INIT;
		g_value_init(&asd, GST_TYPE_ARRAY);
		if(element->asd) {
			guint m;
			for(m = 0; m < element->fft_samples / 2 + 1; m++) {
				GValue asd_sample = G_VALUE_INIT;
				g_value_init(&asd_sample, G_TYPE_DOUBLE);
				g_value_set_double(&asd_sample, element->asd[m]);
				gst_value_array_append_value(&asd, &asd_sample);
				g_value_unset(&asd_sample);
			}
		}
		g_value_copy(&asd, value);
		g_value_unset(&asd);

		break;

	case ARG_ASD_ENDTIME:
		g_value_set_uint64(value, element->asd_endtime);
		break;

	case ARG_FILENAME:
		g_value_set_string(value, element->filename);
		break;

	case ARG_WHITEN_FILTER: ;
		GValue filter = G_VALUE_INIT;
		g_value_init(&filter, GST_TYPE_ARRAY);
		if(element->whiten_filter) {
			guint n;
			for(n = 0; n < element->filter_samples ; n++) {
				GValue filter_sample = G_VALUE_INIT;
				g_value_init(&filter_sample, G_TYPE_DOUBLE);
				g_value_set_double(&filter_sample, element->whiten_filter[n]);
				gst_value_array_append_value(&filter, &filter_sample);
				g_value_unset(&filter_sample);
			}
		}
		g_value_copy(&filter, value);
		g_value_unset(&filter);

		break;

	case ARG_MEDIAN_SAMPLES:
		g_value_set_uint64(value, element->median_samples);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
		break;
	}

	GST_OBJECT_UNLOCK(element);
}


/*
 * class_init()
 */


#define CAPS \
	"audio/x-raw, " \
	"rate = (int) [1, MAX], " \
	"channels = (int) 1, " \
	"format = (string) {"GST_AUDIO_NE(F32)", "GST_AUDIO_NE(F64)"}, " \
	"layout = (string) interleaved, " \
	"channel-mask = (bitmask) 0"


static void gstlal_asd_class_init(GSTLALASDClass *klass) {

	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
	GstBaseSinkClass *gstbasesink_class = GST_BASE_SINK_CLASS(klass);

	gstbasesink_class->set_caps = GST_DEBUG_FUNCPTR(set_caps);
	gstbasesink_class->event = GST_DEBUG_FUNCPTR(event);
	gstbasesink_class->render = GST_DEBUG_FUNCPTR(render);
	gstbasesink_class->stop = GST_DEBUG_FUNCPTR(stop);

	gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
	gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);

	gst_element_class_set_details_simple(
		element_class,
		"Compute an ASD of a stream",
		"Sink",
		"Compute an ASD of a single-channel stream.  As long as there is input data\n\t\t\t   "
		"(i.e., no gap), data will be used in the average.  Only one ASD is computed,\n\t\t\t   "
		"and it will be finished at the end of the stream and written to file. The\n\t\t\t   "
		"file contains two columns: a frequency vector and the ASD.",
		"Aaron Viets <aaron.viets@ligo.org>"
	);

	gst_element_class_add_pad_template(
		element_class,
		gst_pad_template_new(
			"sink",
			GST_PAD_SINK,
			GST_PAD_ALWAYS,
			gst_caps_from_string(CAPS)
		)
	);


	properties[ARG_FFT_SAMPLES] = g_param_spec_uint64(
		"fft-samples",
		"FFT Samples",
		"Number of input samples to use in each FFT",
		1, G_MAXUINT64, 16384,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_OVERLAP_SAMPLES] = g_param_spec_uint64(
		"overlap-samples",
		"Overlap Samples",
		"Number of input samples of overlap between consecutive FFTs",
		0, G_MAXUINT64, 8192,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_UPDATE_TIME] = g_param_spec_uint64(
		"update-time",
		"Update Time",
		"Time in seconds between each update of the average ASD",
		0, G_MAXUINT64, G_MAXUINT64,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_WINDOW_TYPE] = g_param_spec_enum(
		"window-type",
		"Window Function Type",
		"What window function to apply to input samples before computing FFTs",
		GSTLAL_ASD_WINDOW_TYPE,
		GSTLAL_ASD_DPSS,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_FREQUENCY_RESOLUTION] = g_param_spec_double(
		"frequency-resolution",
		"Frequency Resolution of ASD",
		"This parameter sets the frequency resolution (in Hz) of the window function\n\t\t\t"
		"applied to the input data.  It must be greater than the inverse of the\n\t\t\t"
		"length of the filter in seconds; otherwise, it will be overridden.  If unset\n\t\t\t"
		"or set to zero (default), the frequency resolution will be reset to 3\n\t\t\t"
		"frequency bins, or 3 times the inverse of the temporal duration of the\n\t\t\t"
		"FFT length.",
		0.0, G_MAXDOUBLE, 0.0,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_ASD] = gst_param_spec_array(
		"asd",
		"ASD",
		"The computed ASD",
		g_param_spec_double(
			"sample",
			"Sample",
			"A sample from the ASD",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_ASD_ENDTIME] = g_param_spec_uint64(
		"asd-endtime",
		"ASD end time",
		"The time when a computed ASD ceases to be valid",
		0, G_MAXUINT64, G_MAXUINT64,
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_FILENAME] = g_param_spec_string(
		"filename",
		"Filename",
		"Name of file in which to write ASD.  Default is 'asd.txt'",
		"asd.txt",
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);
	properties[ARG_WHITEN_FILTER] = gst_param_spec_array(
		"whiten-filter",
		"Whitening Filter",
		"A whitening filter produced from the computed ASD",
		g_param_spec_double(
			"sample",
			"Sample",
			"A sample from the filter",
			-G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		),
		G_PARAM_READABLE | G_PARAM_STATIC_STRINGS
	);
	properties[ARG_MEDIAN_SAMPLES] = g_param_spec_uint64(
		"median-samples",
		"Median Samples",
		"How many ASD samples to use in a median before computing the whitening filter.\n\t\t\t"
		"The whitening filter is downsampled by this factor in the frequency domain.",
		0, G_MAXUINT64, 64,
		G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT
	);


	g_object_class_install_property(
		gobject_class,
		ARG_FFT_SAMPLES,
		properties[ARG_FFT_SAMPLES]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_OVERLAP_SAMPLES,
		properties[ARG_OVERLAP_SAMPLES]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_UPDATE_TIME,
		properties[ARG_UPDATE_TIME]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_WINDOW_TYPE,
		properties[ARG_WINDOW_TYPE]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FREQUENCY_RESOLUTION,
		properties[ARG_FREQUENCY_RESOLUTION]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_ASD,
		properties[ARG_ASD]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_ASD_ENDTIME,
		properties[ARG_ASD_ENDTIME]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_FILENAME,
		properties[ARG_FILENAME]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_WHITEN_FILTER,
		properties[ARG_WHITEN_FILTER]
	);
	g_object_class_install_property(
		gobject_class,
		ARG_MEDIAN_SAMPLES,
		properties[ARG_MEDIAN_SAMPLES]
	);
}


/*
 * init()
 */


static void gstlal_asd_init(GSTLALASD *element) {

	element->rate = 0;
	element->unit_size = 0;
	element->t0 = GST_CLOCK_TIME_NONE;
	element->offset0 = GST_BUFFER_OFFSET_NONE;
	element->next_in_offset = GST_BUFFER_OFFSET_NONE;
	element->fft_samples = 0;
	element->overlap_samples = 0;
	element->window_type = 0;
	element->filename = NULL;
	element->input_index = 0;
	element->num_ffts_in_avg = 0;

	gst_base_sink_set_sync(GST_BASE_SINK(element), FALSE);
	gst_base_sink_set_async_enabled(GST_BASE_SINK(element), FALSE);
}

