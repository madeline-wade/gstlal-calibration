# Installing gstlal-calibration

## Install from source in a conda environment

It is recommended that you install from source within an `igwn` conda environment with python 3.9 or less.  The recommended environment is `igwn-py39`, which can be activated with
```shell
conda activate igwn-py39
```
You can install from source by first cloning the git repository.
```shell
git clone <gstlal-calibration git repository address>
cd gstlal-calibration
```

Next install gstlal-calibration from source within the conda environment.
```shell
./00init.sh
./configure --prefix=/path/to/install
make
make install
```

You will need to add the following to your environment to activate this installation:

```shell
INSTALLPATH=/path/to/install

PATH=${INSTALLPATH}/bin:$PATH
PKG_CONFIG_PATH=${INSTALLPATH}/lib/pkgconfig:${INSTALLPATH}/lib64/pkgconfig:$PKG_CONFIG_PATH
PYTHONPATH=${INSTALLPATH}/lib/python3.9/site-packages:${INSTALLPATH}/lib64/python3.9/site-packages
GST_PLUGIN_PATH=${INSTALLPATH}/lib/gstreamer-1.0:${INSTALLPATH}/lib64/gstreamer-1.0:$GST_PLUGIN_PATH
GST_REGISTRY_1_0=${INSTALLPATH}/registry.bin

export PATH PKG_CONFIG_PATH PYTHONPATH GST_PLUGIN_PATH GST_REGISTRY_1_0
```
